#include "unity.h"

#include <math.h>

#define TEST_ASSERT_DOUBLE_IS_NEG_ZERO(actual) TEST_ASSERT_MESSAGE((0.0f == (actual)) && (0 != signbit(actual)), "Expected -0.0f")
#define TEST_ASSERT_DOUBLE_IS_ZERO(actual) TEST_ASSERT_MESSAGE((0.0f == (actual)) && (0 == signbit(actual)), "Expected 0.0f")

double deg2rad(double x) {
    if (x == -0.0f && signbit(x) != 0) return -0.0f;
    return (double)(x * (M_PI/180.0));
}

double rem2pi(double x) {
    double dx = x;
    return (double)fmod(dx, M_PI_2);
}

void test_math_double_special_acos(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(acos(-INFINITY));

    //. x < -1 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(acos(-12.3));

    //. x in [-1; +1[ -> acos(x)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.359d26f93b6c3p1, acos(-0.75));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.0c152382d7366p1, acos(-0.5));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.921fb54442d18p0, acos(0.0));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.51700e0c14b25p0, acos(0.25));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.dac670561bb50p-1, acos(0.6));

    //. x = +1 -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(acos(1.0));

    //. x > +1 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(acos(12.3));

    //. x = +Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(acos(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(acos(NAN));
}

void test_math_double_special_asin(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(asin(-INFINITY));

    //. x < -1 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(asin(-12.3));

    //. x in [-1; -0[ -> asin(x)
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.b235315c680dcp-1, asin(-0.75));
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.0c152382d7366p-1, asin(-0.5));
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.9a49276037884p-4, asin(-0.1));

    //. x = -0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(asin(-0.0));

    //. x = +0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(asin(0.0));

    //. x in ]+0; +1] -> asin(x)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.9a49276037884p-4, asin(0.1));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.02be9ce0b87cdp-2, asin(0.25));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.4978fa3269ee1p-1, asin(0.6));

    //. x > +1 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(asin(12.3));

    //. x = +Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(asin(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(asin(NAN));
}

void test_math_double_special_atan(void) {
    //. x = -Inf -> -π/2
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI_2, atan(-INFINITY));

    //. x < 0 -> atan(x)
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.900ae8244749ep0, atan(-123.0));
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.7d5b455f36dfbp0, atan(-12.3));
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.f54a44d6ee2bcp-4, atan(-0.123));

    //. x = -0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(atan(-0.0));

    //. x = +0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(atan(0.0));

    //. x > 0 -> atan(x)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.4781623768d56p-5, atan(0.04));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.b1009e9ee79bcp-2, atan(0.45));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.5adbbda1ef194p0, atan(4.56));

    //. x = +Inf -> +π/2
    TEST_ASSERT_EQUAL_DOUBLE(M_PI_2, atan(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan(NAN));
}

void test_math_double_special_atan2(void) {
    //. -- y = -Inf --
    //. y = -Inf; x = -Inf -> -3/4π
    TEST_ASSERT_EQUAL_DOUBLE(-3.0f * M_PI_4, atan2(-INFINITY, -INFINITY));

    //. y = -Inf; x < 0 -> -π/2
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI_2, atan2(-INFINITY, -123.0));

    //. y = -Inf; x = -0 -> -π/2
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI_2, atan2(-INFINITY, -0.0));

    //. y = -Inf; x = +0 -> -π/2
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI_2, atan2(-INFINITY, 0.0));

    //. y = -Inf; x > 0 -> -π/2
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI_2, atan2(-INFINITY, 123.0));

    //. y = -Inf; x = +Inf -> -π/4
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI_4, atan2(-INFINITY, INFINITY));

    //. y = -Inf; x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(-INFINITY, NAN));


    //. -- y < 0 --
    //. y < 0; x = -Inf -> -π
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI, atan2(-123.0, -INFINITY));

    //. y < 0; x < 0 -> atan(y/x)
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.2d97c7f3321d2p1, atan2(-123.0, -123.0));

    //. y < 0; x = -0 -> -π/2
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI_2, atan2(-123.0, -0.0));

    //. y < 0; x = +0 -> -π/2
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI_2, atan2(-123.0, 0.0));

    //. y < 0; x > 0 -> atan(y/x)
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI, atan2(-123.0, -INFINITY));

    //. y < 0; x = +Inf -> -0
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(atan2(-123.0, INFINITY));

    //. y < 0; x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(-123.0, NAN));


    //. -- y = -0 --
    //. y = -0; x = -Inf -> -π
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI, atan2(-0.0, -INFINITY));

    //. y = -0; x < 0 -> -π
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI, atan2(-0.0, -123.0));

    //. y = -0; x = -0 -> -π
    TEST_ASSERT_EQUAL_DOUBLE(-M_PI, atan2(-0.0, -0.0));

    //. y = -0; x = +0 -> -0
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(atan2(-0.0, 0.0));

    //. y = -0; x > 0 -> -0
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(atan2(-0.0, 123.0));

    //. y = -0; x = +Inf -> -0
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(atan2(-0.0, INFINITY));

    //. y = -0; x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(-0.0, NAN));


    //. -- y = +0 --
    //. y = +0; x = -Inf -> +π
    TEST_ASSERT_EQUAL_DOUBLE(M_PI, atan2(0.0, -INFINITY));

    //. y = +0; x < 0 -> +π
    TEST_ASSERT_EQUAL_DOUBLE(M_PI, atan2(0.0, -123.0));

    //. y = +0; x = -0 -> +π
    TEST_ASSERT_EQUAL_DOUBLE(M_PI, atan2(0.0, -0.0));

    //. y = +0; x = +0 -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(atan2(0.0, 0.0));

    //. y = +0; x > 0 -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(atan2(0.0, 123.0));

    //. y = +0; x = +Inf -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(atan2(0.0, INFINITY));

    //. y = +0; x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(0.0, NAN));


    //. -- y > 0 --
    //. y > 0; x = -Inf -> +π
    TEST_ASSERT_EQUAL_DOUBLE(M_PI, atan2(123.0, -INFINITY));

    //. y > 0; x < 0 -> atan(y/x)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.2d97c7f3321d2p1, atan2(123.0, -123.0));

    //. y > 0; x = -0 -> +π/2
    TEST_ASSERT_EQUAL_DOUBLE(M_PI_2, atan2(123.0, -0.0));

    //. y > 0; x = +0 -> +π/2
    TEST_ASSERT_EQUAL_DOUBLE(M_PI_2, atan2(123.0, 0.0));

    //. y > 0; x > 0 -> atan(y/x)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.921fb54442d18p-1, atan2(123.0, 123.0));

    //. y > 0; x = +Inf -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(atan2(123.0, INFINITY));

    //. y > 0; x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(123.0, NAN));


    //. -- y = +Inf --
    //. y = +Inf; x = -Inf -> 3/4π
    TEST_ASSERT_EQUAL_DOUBLE(3.0f * M_PI_4, atan2(INFINITY, -INFINITY));

    //. y = +Inf; x < 0 -> +π/2
    TEST_ASSERT_EQUAL_DOUBLE(M_PI_2, atan2(INFINITY, -123.0));

    //. y = +Inf; x = -0 -> +π/2
    TEST_ASSERT_EQUAL_DOUBLE(M_PI_2, atan2(INFINITY, -0.0));

    //. y = +Inf; x = +0 -> +π/2
    TEST_ASSERT_EQUAL_DOUBLE(M_PI_2, atan2(INFINITY, 0.0));

    //. y = +Inf; x > 0 -> +π/2
    TEST_ASSERT_EQUAL_DOUBLE(M_PI_2, atan2(INFINITY, 123.0));

    //. y = +Inf; x = +Inf -> π/4
    TEST_ASSERT_EQUAL_DOUBLE(M_PI_4, atan2(INFINITY, INFINITY));

    //. y = +Inf; x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(INFINITY, NAN));


    //. -- y = NaN --
    //. y = NaN; x = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(NAN, -INFINITY));

    //. y = NaN; x < 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(NAN, -123.0));

    //. y = NaN; x = -0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(NAN, -0.0));

    //. y = NaN; x = +0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(NAN, 0.0));

    //. y = NaN; x > 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(NAN, 123.0));

    //. y = NaN; x = +Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(NAN, INFINITY));

    //. y = NaN; x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(atan2(NAN, NAN));
}

void test_math_double_special_ceil(void) {
    //. x = -Inf -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(ceil(-INFINITY));

    //. x < 0 -> ceil(x)
    TEST_ASSERT_EQUAL_DOUBLE(-123.0, ceil(-123.456));
    TEST_ASSERT_EQUAL_DOUBLE(-12.0, ceil(-12.3));
    TEST_ASSERT_EQUAL_DOUBLE(-1.0, ceil(-1.23456));

    //. x = -0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(ceil(-0.0));

    //. x = +0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(ceil(0.0));

    //. x > 0 -> ceil(x)
    TEST_ASSERT_EQUAL_DOUBLE(2.0, ceil(1.23456));
    TEST_ASSERT_EQUAL_DOUBLE(13.0, ceil(12.3));
    TEST_ASSERT_EQUAL_DOUBLE(124.0, ceil(123.456));

    //. x = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(ceil(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(ceil(NAN));
}

void test_math_double_special_copysign(void) {
    //. -- x /= NaN --
    //. x /= NaN ; y = -NaN -> -|x|
    TEST_ASSERT_EQUAL_DOUBLE(-1.23456, copysign(1.23456, -NAN));

    //. x /= NaN ; y = -Inf -> -|x|
    TEST_ASSERT_EQUAL_DOUBLE(-2.34561, copysign(-2.34561, -INFINITY));

    //. x /= NaN ; y < 0 -> -|x|
    TEST_ASSERT_EQUAL_DOUBLE(-3.45612, copysign(3.45612, -23.0));

    //. x /= NaN ; y = -0 -> -|x|
    TEST_ASSERT_EQUAL_DOUBLE(-4.56123, copysign(-4.56123, -0.0));

    //. x /= NaN ; y = +0 -> |x|
    TEST_ASSERT_EQUAL_DOUBLE(4.56123, copysign(-4.56123, 0.0));

    //. x /= NaN ; y > 0 -> |x|
    TEST_ASSERT_EQUAL_DOUBLE(3.45612, copysign(3.45612, 123.0));

    //. x /= NaN ; y = +Inf -> |x|
    TEST_ASSERT_EQUAL_DOUBLE(2.34561, copysign(-2.34561, INFINITY));

    //. x /= NaN ; y = +NaN -> |x|
    TEST_ASSERT_EQUAL_DOUBLE(1.23456, copysign(1.23456, NAN));


    //. -- x = NaN --
    //. x = NaN ; y = -NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(copysign(NAN, -NAN));

    //. x = NaN ; y = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(copysign(NAN, -INFINITY));

    //. x = NaN ; y < 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(copysign(NAN, -123.0));

    //. x = NaN ; y = -0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(copysign(NAN, -0.0));

    //. x = NaN ; y = +0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(copysign(NAN, 0.0));

    //. x = NaN ; y > 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(copysign(NAN, 23.0));

    //. x = NaN ; y = +Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(copysign(NAN, INFINITY));

    //. x = NaN ; y = +NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(copysign(NAN, NAN));
}

void test_math_double_special_cos(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(cos(-INFINITY));

    //. x < 0 -> cos(x)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.56426a2c5b3fep-2, cos(-1.23));

    //. x = -0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, cos(-0.0));

    //. x = +0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, cos(0.0));

    //. x > 0 -> cos(x)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.edf16f066a376p-1, cos(12.3));

    //. x = +Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(cos(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(cos(NAN));
}

void test_math_double_special_deg2rad(void) {
    //. x = -Inf -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(deg2rad(-INFINITY));

    //. x < 0 -> x in rad
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.12c8ddffb6315p+1, deg2rad(-123.0));
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.b7a7c99923822p-3, deg2rad(-12.3));
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.5fb96e141c681p-6, deg2rad(-1.23));

    //. x = -0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(deg2rad(-0.0));

    //. x = +0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(deg2rad(0.0));

    //. x > 0 -> x in rad
    TEST_ASSERT_EQUAL_DOUBLE(0x1.196124dce3867p-9, deg2rad(0.123));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.5fb96e141c681p-6, deg2rad(1.23));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.b7a7c99923822p-3, deg2rad(12.3));

    //. x = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(deg2rad(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(deg2rad(NAN));
}

void test_math_double_special_exp(void) {
    //. x = -Inf -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(exp(-INFINITY));

    //. x < 0 -> e^x
    TEST_ASSERT_EQUAL_DOUBLE(0x1.766b45dd84f18p-178, exp(-123.0));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.31765fdaf4f37p-18, exp(-12.3));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.2b4ebed802e61p-2, exp(-1.23));

    //. x = -0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, exp(-0.0));

    //. x = +0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, exp(0.0));

    //. x > 0 -> e^x
    TEST_ASSERT_EQUAL_DOUBLE(0x1.2181a433867e4p+0, exp(0.123));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.b5ead9753c982p+1, exp(1.23));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.ad17fe8ccf016p+17, exp(12.3));

    //. x = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(exp(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(exp(NAN));
}

void test_math_double_special_fabs(void) {
    //. x = -Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(fabs(-INFINITY));

    //. x < 0 -> |x|
    TEST_ASSERT_EQUAL_DOUBLE(123.0, fabs(-123.0));
    TEST_ASSERT_EQUAL_DOUBLE(12.3, fabs(-12.3));
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fabs(-1.23));

    //. x = -0 -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(fabs(-0.0));

    //. x = +0 -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(fabs(0.0));

    //. x > 0 -> x
    TEST_ASSERT_EQUAL_DOUBLE(0.123, fabs(0.123));
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fabs(1.23));
    TEST_ASSERT_EQUAL_DOUBLE(12.3, fabs(12.3));

    //. x = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(fabs(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fabs(NAN));
}

void test_math_double_special_floor(void) {
    //. x = -Inf -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(floor(-INFINITY));

    //. x < 0 -> floor(x)
    TEST_ASSERT_EQUAL_DOUBLE(-123.0, floor(-123.0));
    TEST_ASSERT_EQUAL_DOUBLE(-13.0, floor(-12.3));
    TEST_ASSERT_EQUAL_DOUBLE(-2.00, floor(-1.23));

    //. x = -0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(floor(-0.0));

    //. x = +0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(floor(0.0));

    //. x > 0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(floor(0.123));
    TEST_ASSERT_EQUAL_DOUBLE(1.0, floor(1.23));
    TEST_ASSERT_EQUAL_DOUBLE(12.0, floor(12.3));

    //. x = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(floor(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(floor(NAN));
}

void test_math_double_special_fmax(void) {
    //. -- x = -Inf --
    //. x = -Inf ; y = -Inf -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmax(-INFINITY, -INFINITY));

    //. x = -Inf ; y < 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmax(-INFINITY, -12.3));

    //. x = -Inf ; y = -0 -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmax(-INFINITY, -0.0));

    //. x = -Inf ; y = +0 -> y
    TEST_ASSERT_DOUBLE_IS_ZERO(fmax(-INFINITY, 0.0));

    //. x = -Inf ; y > 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(12.3, fmax(-INFINITY, 12.3));

    //. x = -Inf ; y = +Inf -> y
    TEST_ASSERT_DOUBLE_IS_INF(fmax(-INFINITY, INFINITY));

    //. x = -Inf ; y = NaN -> x
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmax(-INFINITY, NAN));


    //. -- x < 0 --
    //. x < 0 ; y = -Inf -> x
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmax(-12.3, -INFINITY));

    //. x < 0 ; y < 0 -> max(x, y)
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmax(-12.3, -123.0));
    TEST_ASSERT_EQUAL_DOUBLE(-1.23, fmax(-12.3, -1.23));

    //. x < 0 ; y = -0 -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmax(-12.3, -0.0));

    //. x < 0 ; y = +0 -> y
    TEST_ASSERT_DOUBLE_IS_ZERO(fmax(-12.3, 0.0));

    //. x < 0 ; y > 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(3.45, fmax(-12.3, 3.45));

    //. x < 0 ; y = +Inf -> y
    TEST_ASSERT_EQUAL_DOUBLE(INFINITY, fmax(-12.3, INFINITY));

    //. x < 0 ; y = NaN -> x
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmax(-12.3, NAN));


    //. -- x = -0 --
    //. x = -0 ; y = -Inf -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmax(-0.0, -INFINITY));

    //. x = -0 ; y < 0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmax(-0.0, -12.3));

    //. x = -0 ; y = -0 -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmax(-0.0, -INFINITY));

    //. x = -0 ; y = +0 -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmax(-0.0, -INFINITY));

    //. x = -0 ; y > 0 -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmax(-0.0, -INFINITY));

    //. x = -0 ; y = +Inf -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmax(-0.0, -INFINITY));

    //. x = -0 ; y = NaN -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmax(-0.0, -INFINITY));


    //. -- x = +0 --
    //. x = +0 ; y = -Inf -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(fmax(0.0, -INFINITY));

    //. x = +0 ; y < 0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(fmax(0.0, -12.3));

    //. x = +0 ; y = -0 -> y
    TEST_ASSERT_DOUBLE_IS_ZERO(fmax(0.0, -0.0));

    //. x = +0 ; y = +0 -> y
    TEST_ASSERT_DOUBLE_IS_ZERO(fmax(0.0, 0.0));

    //. x = +0 ; y > 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(12.3, fmax(0.0, 12.3));

    //. x = +0 ; y = +Inf -> y
    TEST_ASSERT_EQUAL_DOUBLE(INFINITY, fmax(0.0, INFINITY));

    //. x = +0 ; y = NaN -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(fmax(0.0, NAN));


    //. -- x > 0 --
    //. x > 0 ; y = -Inf -> x
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fmax(1.23, NAN));

    //. x > 0 ; y < 0 -> x
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fmax(1.23, -12.3));

    //. x > 0 ; y = -0 -> x
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fmax(1.23, -0.0));

    //. x > 0 ; y = +0 -> x
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fmax(1.23, 0.0));

    //. x > 0 ; y > 0 -> max(x, y)
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fmax(1.23, 0.123));
    TEST_ASSERT_EQUAL_DOUBLE(12.3, fmax(1.23, 12.3));

    //. x > 0 ; y = +Inf -> y
    TEST_ASSERT_DOUBLE_IS_INF(fmax(1.23, INFINITY));

    //. x > 0 ; y = NaN -> x
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fmax(1.23, NAN));


    //. -- x = +Inf --
    //. x = +Inf ; y = -Inf -> x
    TEST_ASSERT_DOUBLE_IS_INF(fmax(INFINITY, -INFINITY));

    //. x = +Inf ; y < 0 -> x
    TEST_ASSERT_DOUBLE_IS_INF(fmax(INFINITY, -12.3));

    //. x = +Inf ; y = -0 -> x
    TEST_ASSERT_DOUBLE_IS_INF(fmax(INFINITY, -0.0));

    //. x = +Inf ; y = +0 -> x
    TEST_ASSERT_DOUBLE_IS_INF(fmax(INFINITY, 0.0));

    //. x = +Inf ; y > 0 -> x
    TEST_ASSERT_DOUBLE_IS_INF(fmax(INFINITY, 12.3));

    //. x = +Inf ; y = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(fmax(INFINITY, INFINITY));

    //. x = +Inf ; y = NaN -> x
    TEST_ASSERT_DOUBLE_IS_INF(fmax(INFINITY, NAN));


    //. -- x = NaN --
    //. x = NaN ; y = -Inf -> y
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmax(NAN, -INFINITY));

    //. x = NaN ; y < 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmax(NAN, -12.3));

    //. x = NaN ; y = -0 -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmax(NAN, -0.0));

    //. x = NaN ; y = +0 -> y
    TEST_ASSERT_DOUBLE_IS_ZERO(fmax(NAN, 0.0));

    //. x = NaN ; y > 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(12.3, fmax(NAN, 12.3));

    //. x = NaN ; y = +Inf -> y
    TEST_ASSERT_DOUBLE_IS_INF(fmax(NAN, INFINITY));

    //. x = NaN ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmax(NAN, NAN));
}

void test_math_double_special_fmin(void) {
    //. -- x = -Inf --
    //. x = -Inf ; y = -Inf -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(-INFINITY, -INFINITY));

    //. x = -Inf ; y < 0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(-INFINITY, -12.3));

    //. x = -Inf ; y = -0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(-INFINITY, -0.0));

    //. x = -Inf ; y = +0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(-INFINITY, 0.0));

    //. x = -Inf ; y > 0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(-INFINITY, 12.3));

    //. x = -Inf ; y = +Inf -> x
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(-INFINITY, INFINITY));

    //. x = -Inf ; y = NaN -> x
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(-INFINITY, NAN));


    //. -- x < 0 --
    //. x < 0 ; y = -Inf -> y
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(-12.3, -INFINITY));

    //. x < 0 ; y < 0 -> min(x, y)
    TEST_ASSERT_EQUAL_DOUBLE(-123.0, fmin(-12.3, -123.0));
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmin(-12.3, -1.23));

    //. x < 0 ; y = -0 -> x
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmin(-12.3, -0.0));

    //. x < 0 ; y = +0 -> x
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmin(-12.3, 0.0));

    //. x < 0 ; y > 0 -> x
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmin(-12.3, 3.45));

    //. x < 0 ; y = +Inf -> x
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmin(-12.3, INFINITY));

    //. x < 0 ; y = NaN -> x
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmin(-12.3, NAN));


    //. -- x = -0 --
    //. x = -0 ; y = -Inf -> y
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(-0.0, -INFINITY));

    //. x = -0 ; y < 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmin(-0.0, -12.3));

    //. x = -0 ; y = -0 -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmin(-0.0, -0.0));

    //. x = -0 ; y = +0 -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmin(-0.0, 0.0));

    //. x = -0 ; y > 0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmin(-0.0, 12.3));

    //. x = -0 ; y = +Inf -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmin(-0.0, INFINITY));

    //. x = -0 ; y = NaN -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmin(-0.0, NAN));


    //. -- x = +0 --
    //. x = +0 ; y = -Inf -> y
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(0.0, -INFINITY));

    //. x = +0 ; y < 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmin(0.0, -12.3));

    //. x = +0 ; y = -0 -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmin(0.0, -0.0));

    //. x = +0 ; y = +0 -> y
    TEST_ASSERT_DOUBLE_IS_ZERO(fmin(0.0, 0.0));

    //. x = +0 ; y > 0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(fmin(0.0, 12.3));

    //. x = +0 ; y = +Inf -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(fmin(0.0, INFINITY));

    //. x = +0 ; y = NaN -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(fmin(0.0, NAN));


    //. -- x > 0 --
    //. x > 0 ; y = -Inf -> y
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(1.23, -INFINITY));

    //. x > 0 ; y < 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmin(1.23, -12.3));

    //. x > 0 ; y = -0 -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmin(1.23, -0.0));

    //. x > 0 ; y = +0 -> y
    TEST_ASSERT_DOUBLE_IS_ZERO(fmin(1.23, 0.0));

    //. x > 0 ; y > 0 -> min(x, y)
    TEST_ASSERT_EQUAL_DOUBLE(0.123, fmin(1.23, 0.123));
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fmin(1.23, 12.3));

    //. x > 0 ; y = +Inf -> x
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fmin(1.23, INFINITY));

    //. x > 0 ; y = NaN -> x
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fmin(1.23, NAN));


    //. -- x = +Inf --
    //. x = +Inf ; y = -Inf -> y
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(INFINITY, -INFINITY));

    //. x = +Inf ; y < 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmin(INFINITY, -12.3));

    //. x = +Inf ; y = -0 -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmin(INFINITY, -0.0));

    //. x = +Inf ; y = +0 -> y
    TEST_ASSERT_DOUBLE_IS_ZERO(fmin(INFINITY, 0.0));

    //. x = +Inf ; y > 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(12.3, fmin(INFINITY, 12.3));

    //. x = +Inf ; y = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(fmin(INFINITY, INFINITY));

    //. x = +Inf ; y = NaN -> x
    TEST_ASSERT_DOUBLE_IS_INF(fmin(INFINITY, NAN));


    //. -- x = NaN --
    //. x = NaN ; y = -Inf -> y
    TEST_ASSERT_DOUBLE_IS_NEG_INF(fmin(NAN, -INFINITY));

    //. x = NaN ; y < 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmin(NAN, -12.3));

    //. x = NaN ; y = -0 -> y
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmin(NAN, -0.0));

    //. x = NaN ; y = +0 -> y
    TEST_ASSERT_DOUBLE_IS_ZERO(fmin(NAN, 0.0));

    //. x = NaN ; y > 0 -> y
    TEST_ASSERT_EQUAL_DOUBLE(12.3, fmin(NAN, 12.3));

    //. x = NaN ; y = +Inf -> y
    TEST_ASSERT_DOUBLE_IS_INF(fmin(NAN, INFINITY));

    //. x = NaN ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmin(NAN, NAN));
}

void test_math_double_special_fmod(void) {
    //. -- x = -Inf --
    //. x = -Inf ; y = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-INFINITY, -INFINITY));

    //. x = -Inf ; y < 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-INFINITY, -12.3));

    //. x = -Inf ; y = -0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-INFINITY, -0.0));

    //. x = -Inf ; y = +0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-INFINITY, 0.0));

    //. x = -Inf ; y > 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-INFINITY, 12.3));

    //. x = -Inf ; y = +Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-INFINITY, INFINITY));

    //. x = -Inf ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-INFINITY, NAN));


    //. -- x < 0 --
    //. x < 0 ; y = -Inf -> x
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmod(-12.3, -INFINITY));

    //. x < 0 ; y < 0 -> fmod(x, y)
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmod(-12.3, -123.0));
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.8999999999996p+3, fmod(-123.0, -12.3));

    //. x < 0 ; y = -0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-12.3, -0.0));

    //. x < 0 ; y = +0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-12.3, 0.0));

    //. x < 0 ; y > 0 -> fmod(x, y)
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmod(-12.3, 123.0));
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.8999999999996p+3, fmod(-123.0, 12.3));

    //. x < 0 ; y = +Inf -> x
    TEST_ASSERT_EQUAL_DOUBLE(-12.3, fmod(-12.3, INFINITY));

    //. x < 0 ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-12.3, NAN));


    //. -- x = -0 --
    //. x = -0 ; y = -Inf -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmod(-0.0, -INFINITY));

    //. x = -0 ; y < 0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmod(-0.0, -12.3));

    //. x = -0 ; y = -0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-0.0, -0.0));

    //. x = -0 ; y = +0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-0.0, 0.0));

    //. x = -0 ; y > 0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmod(-0.0, 12.3));

    //. x = -0 ; y = +Inf -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(fmod(-0.0, INFINITY));

    //. x = -0 ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(-0.0, NAN));


    //. -- x = +0 --
    //. x = +0 ; y = -Inf -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(fmod(0.0, -INFINITY));

    //. x = +0 ; y < 0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(fmod(0.0, -12.3));

    //. x = +0 ; y = -0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(0.0, -0.0));

    //. x = +0 ; y = +0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(0.0, 0.0));

    //. x = +0 ; y > 0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(fmod(0.0, 12.3));

    //. x = +0 ; y = +Inf -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(fmod(0.0, INFINITY));

    //. x = +0 ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(0.0, NAN));


    //. -- x > 0 --
    //. x > 0 ; y = -Inf -> x
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fmod(1.23, -INFINITY));

    //. x > 0 ; y < 0 -> fmod(x, y)
    TEST_ASSERT_EQUAL_DOUBLE(12.3, fmod(12.3, -123.0));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.8999999999996p+3, fmod(123.0, -12.3));

    //. x > 0 ; y = -0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(1.23, -0.0));

    //. x > 0 ; y = +0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(1.23, 0.0));

    //. x > 0 ; y > 0 -> fmod(x, y)
    TEST_ASSERT_EQUAL_DOUBLE(12.3, fmod(12.3, 123.0));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.8999999999996p+3, fmod(123.0, 12.3));

    //. x > 0 ; y = +Inf -> x
    TEST_ASSERT_EQUAL_DOUBLE(1.23, fmod(1.23, INFINITY));

    //. x > 0 ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(1.23, NAN));


    //. -- x = +Inf --
    //. x = +Inf ; y = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(INFINITY, -INFINITY));

    //. x = +Inf ; y < 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(INFINITY, -12.3));

    //. x = +Inf ; y = -0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(INFINITY, -0.0));

    //. x = +Inf ; y = +0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(INFINITY, 0.0));

    //. x = +Inf ; y > 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(INFINITY, 12.3));

    //. x = +Inf ; y = +Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(INFINITY, INFINITY));

    //. x = +Inf ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(INFINITY, NAN));


    //. -- x = NaN --
    //. x = NaN ; y = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(NAN, -INFINITY));

    //. x = NaN ; y < 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(NAN, -12.3));

    //. x = NaN ; y = -0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(NAN, -0.0));

    //. x = NaN ; y = +0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(NAN, 0.0));

    //. x = NaN ; y > 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(NAN, 12.3));

    //. x = NaN ; y = +Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(NAN, INFINITY));

    //. x = NaN ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(fmod(NAN, NAN));
}

void test_math_double_special_hypot(void) {
    //. -- x = -Inf --
    //. x = -Inf ; y = -Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(hypot(-INFINITY, -INFINITY));

    //. x = -Inf ; y e R -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(hypot(-INFINITY, -123.0));
    TEST_ASSERT_DOUBLE_IS_INF(hypot(-INFINITY, 0.0));
    TEST_ASSERT_DOUBLE_IS_INF(hypot(-INFINITY, 123.0));

    //. x = -Inf ; y = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(hypot(-INFINITY, INFINITY));

    //. x = -Inf ; y = NaN -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(hypot(-INFINITY, NAN));


    //. -- x e R --
    //. x e R ; y = -Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(hypot(123.0, -INFINITY));

    //. x e R ; y e R -> sqrt(x^2 + y^2)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.d84c2deb94946p+8, hypot(-456.0, -123.0));
    TEST_ASSERT_DOUBLE_IS_ZERO(hypot(0.0, 0.0));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.d84c2deb94946p+8, hypot(123.0, 456.0));

    //. x e R ; y = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(hypot(123.0, INFINITY));

    //. x e R ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(hypot(123.0, NAN));


    //. -- x = +Inf --
    //. x = +Inf ; y = -Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(hypot(INFINITY, -INFINITY));

    //. x = +Inf ; y e R -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(hypot(INFINITY, -123.0));
    TEST_ASSERT_DOUBLE_IS_INF(hypot(INFINITY, 0.0));
    TEST_ASSERT_DOUBLE_IS_INF(hypot(INFINITY, 123.0));

    //. x = +Inf ; y = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(hypot(INFINITY, INFINITY));

    //. x = +Inf ; y = NaN -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(hypot(INFINITY, NAN));


    //. -- x = NaN --
    //. x = NaN ; y = -Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(hypot(NAN, -INFINITY));

    //. x = NaN ; y e R -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(hypot(NAN, -123.0));
    TEST_ASSERT_DOUBLE_IS_NAN(hypot(NAN, 0.0));
    TEST_ASSERT_DOUBLE_IS_NAN(hypot(NAN, 123.0));

    //. x = NaN ; y = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(hypot(NAN, INFINITY));

    //. x = NaN ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(hypot(NAN, NAN));
}

void test_math_double_special_log(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(log(-INFINITY));

    //. x < 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(log(-123.0));

    //. x = -0 -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(log(-0.0));

    //. x = +0 -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(log(0.0));

    //. x e ]+0 ; +1[ -> log(x)
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.0c3bab03ad16ep+1, log(0.123));

    //. x = +1 -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(log(1.0));

    //. x > +1 -> log(x)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.413a8c72fd8bep+1, log(12.3));

    //. x = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(log(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(log(NAN));
}

void test_math_double_special_log10(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(log10(-INFINITY));

    //. x < 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(log10(-123.0));

    //. x = -0 -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(log10(-0.0));

    //. x = +0 -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(log10(0.0));

    //. x e ]+0 ; +1[ -> log10(x)
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.d1f7f50d3fc51p-1, log10(0.123));

    //. x = +1 -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(log10(1.0));

    //. x > +1 -> log10(x)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.17040579601d8p+0, log10(12.3));

    //. x = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(log10(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(log10(NAN));
}

void test_math_double_special_modf(void) {
    double i;

    //. x = -Inf -> -0 ; i = -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(modf(-INFINITY, &i));
    TEST_ASSERT_DOUBLE_IS_NEG_INF(i);

    //. x < 0 -> x - fmod(x, 1) ; i = fmod(x, 1)
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.ccccccccccd00p-2, modf(-123.45, &i));
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.ec00000000000p6, i);

    //. x = -0 -> x - fmod(x, 1) ; i = fmod(x, 1)
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(modf(-0.0, &i));
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(i);

    //. x = +0 -> x - fmod(x, 1) ; i = fmod(x, 1)
    TEST_ASSERT_DOUBLE_IS_ZERO(modf(0.0, &i));
    TEST_ASSERT_DOUBLE_IS_ZERO(i);

    //. x > 0 -> x - fmod(x, 1) ; i = fmod(x, 1)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.6147ae147ae20p-2, modf(12.345, &i));
    TEST_ASSERT_EQUAL_DOUBLE(0x1.8000000000000p3, i);

    //. x = +Inf -> 0 ; i = +Inf
    TEST_ASSERT_DOUBLE_IS_ZERO(modf(INFINITY, &i));
    TEST_ASSERT_DOUBLE_IS_INF(i);

    //. x = NaN -> NaN ; i = NaN
    TEST_ASSERT_DOUBLE_IS_NAN(modf(NAN, &i));
    TEST_ASSERT_DOUBLE_IS_NAN(i);
}

void test_math_double_special_pow(void) {
    //. -- x = -Inf --
    //. x = -Inf ; y = -Inf -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(-INFINITY, -INFINITY));

    //. x = -Inf ; y e {2k + 1; k e Z<0} -> -0
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(pow(-INFINITY, -23.0));

    //. x = -Inf ; y e {2k; k e Z<0} -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(-INFINITY, -12.0));

    //. x = -Inf ; y < 0 ; y /e Z -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(-INFINITY, -123.45));

    //. x = -Inf ; y = -0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(-INFINITY, -0.0));

    //. x = -Inf ; y = +0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(-INFINITY, 0.0));

    //. x = -Inf ; y > 0 ; y /e Z -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(-INFINITY, 123.45));

    //. x = -Inf ; y e {2k; k e Z>0} -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(-INFINITY, 12.0));

    //. x = -Inf ; y e {2k - 1; k e Z>0} -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(pow(-INFINITY, 23.0));

    //. x = -Inf ; y = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(-INFINITY, INFINITY));

    //. x = -Inf ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(-INFINITY, NAN));


    //. -- x < -1 --
    //. x < -1 ; y = -Inf -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(-1.23, -INFINITY));

    //. x < -1 ; y e {2k + 1; k e Z<0} -> x^y
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(-1.23, -INFINITY));

    //. x < -1 ; y e {2k; k e Z<0} -> x^y
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(-1.23, -INFINITY));

    //. x < -1 ; y < 0 ; y /e Z -> NaN
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(-1.23, -INFINITY));

    //. x < -1 ; y = -0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(-1.23, -0.0));

    //. x < -1 ; y = +0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(-1.23, 0.0));

    //. x < -1 ; y > 0 ; y /e Z -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(-1.23, 123.45));

    //. x < -1 ; y e {2k; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.7fb79d3f66ee6p+3, pow(-1.23, 12.0));

    //. x < -1 ; y e {2k - 1; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.d39a70f3a0edfp+6, pow(-1.23, 23.0));

    //. x < -1 ; y = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(-1.23, INFINITY));

    //. x < -1 ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(-1.23, NAN));


    //. -- x = -1 --
    //. x = -1 ; y = -Inf -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(-1.0, -INFINITY));

    //. x = -1 ; y e {2k + 1; k e Z<0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.0000000000000p+0, pow(-1.0, -23.0));

    //. x = -1 ; y e {2k; k e Z<0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.0000000000000p+0, pow(-1.0, -12.0));

    //. x = -1 ; y < 0 ; y /e Z -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(-1.0, -123.45));

    //. x = -1 ; y = -0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(-1.0, -0.0));

    //. x = -1 ; y = +0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(-1.0, 0.0));

    //. x = -1 ; y > 0 ; y /e Z -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(-1.0, 123.45));

    //. x = -1 ; y e {2k; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.0000000000000p+0, pow(-1.0, 12.0));

    //. x = -1 ; y e {2k - 1; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.0000000000000p+0, pow(-1.0, 23.0));

    //. x = -1 ; y = +Inf -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(-1.0, INFINITY));

    //. x = -1 ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(-1.0, NAN));


    //. -- x e ]-1; -0[ --
    //. x e ]-1; -0[ ; y = -Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(-0.75, -INFINITY));

    //. x e ]-1; -0[ ; y e {2k + 1; k e Z<0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.75bb89a67c1b8p+9, pow(-0.75, -23.0));

    //. x e ]-1; -0[ ; y e {2k; k e Z<0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.f91bd1b62b9cfp+4, pow(-0.75, -12.0));

    //. x e ]-1; -0[ ; y < 0 ; y /e Z -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(-0.75, -123.45));

    //. x e ]-1; -0[ ; y = -0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(-0.75, -0.0));

    //. x e ]-1; -0[ ; y = +0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(-0.75, 0.0));

    //. x e ]-1; -0[ ; y > 0 ; y /e Z -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(-0.75, 123.45));

    //. x e ]-1; -0[ ; y e {2k; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.037e200000000p-5, pow(-0.75, 12.0));

    //. x e ]-1; -0[ ; y e {2k - 1; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.5eb5ee84b0000p-10, pow(-0.75, 23.0));

    //. x e ]-1; -0[ ; y = +Inf -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(-0.75, INFINITY));

    //. x e ]-1; -0[ ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(-0.75, NAN));


    //. -- x = -0 --
    //. x = -0 ; y = -Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(-0.0, -INFINITY));

    //. x = -0 ; y e {2k + 1; k e Z<0} -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(pow(-0.0, -23.0));

    //. x = -0 ; y e {2k; k e Z<0} -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(-0.0, -12.0));

    //. x = -0 ; y < 0 ; y /e Z -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(-0.0, -123.45));

    //. x = -0 ; y = -0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(-0.0, -0.0));

    //. x = -0 ; y = +0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(-0.0, 0.0));

    //. x = -0 ; y > 0 ; y /e Z -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(-0.0, 123.45));

    //. x = -0 ; y e {2k; k e Z>0} -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(-0.0, 12.0));

    //. x = -0 ; y e {2k - 1; k e Z>0} -> -0
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(pow(-0.0, 23.0));

    //. x = -0 ; y = +Inf -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(-0.0, INFINITY));

    //. x = -0 ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(-0.0, NAN));


    //. -- x = +0 --
    //. x = +0 ; y = -Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(0.0, -INFINITY));

    //. x = +0 ; y e {2k + 1; k e Z<0} -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(0.0, -23.0));

    //. x = +0 ; y e {2k; k e Z<0} -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(0.0, -12.0));

    //. x = +0 ; y < 0 ; y /e Z -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(0.0, -123.45));

    //. x = +0 ; y = -0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(0.0, -0.0));

    //. x = +0 ; y = +0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(0.0, 0.0));

    //. x = +0 ; y > 0 ; y /e Z -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(0.0, 123.45));

    //. x = +0 ; y e {2k; k e Z>0} -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(0.0, 12.0));

    //. x = +0 ; y e {2k - 1; k e Z>0} -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(0.0, 23.0));

    //. x = +0 ; y = +Inf -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(0.0, INFINITY));

    //. x = +0 ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(0.0, NAN));


    //. -- x e ]+0; +1[ --
    //. x e ]+0; +1[ ; y = -Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(0.4, -INFINITY));

    //. x e ]+0; +1[ ; y e {2k + 1; k e Z<0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.52d02c7e14aefp+30, pow(0.4, -23.0));

    //. x e ]+0; +1[ ; y e {2k; k e Z<0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.d1a94a1fffffbp+15, pow(0.4, -12.0));

    //. x e ]+0; +1[ ; y < 0 ; y /e Z -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.327e7e61ae3ccp+16, pow(0.4, -12.3));

    //. x e ]+0; +1[ ; y = -0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(0.4, -0.0));

    //. x e ]+0; +1[ ; y = +0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(0.4, 0.0));

    //. x e ]+0; +1[ ; y > 0 ; y /e Z -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.aba639c796d22p-17, pow(0.4, 12.3));

    //. x e ]+0; +1[ ; y e {2k; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.19799812dea14p-16, pow(0.4, 12.0));

    //. x e ]+0; +1[ ; y e {2k - 1; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.82db34012b25ap-31, pow(0.4, 23.0));

    //. x e ]+0; +1[ ; y = +Inf -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(0.4, INFINITY));

    //. x e ]+0; +1[ ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(0.4, NAN));


    //. -- x = 1 --
    //. x = 1 ; y = -Inf -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(1.0, -INFINITY));

    //. x = 1 ; y e {2k + 1; k e Z<0} -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(1.0, -INFINITY));

    //. x = 1 ; y e {2k; k e Z<0} -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(1.0, -INFINITY));

    //. x = 1 ; y < 0 ; y /e Z -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(1.0, -INFINITY));

    //. x = 1 ; y = -0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(1.0, -0.0));

    //. x = 1 ; y = +0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(1.0, 0.0));

    //. x = 1 ; y > 0 ; y /e Z -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(1.0, 123.45));

    //. x = 1 ; y e {2k; k e Z>0} -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(1.0, 12.0));

    //. x = 1 ; y e {2k - 1; k e Z>0} -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(1.0, 23.0));

    //. x = 1 ; y = +Inf -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(1.0, INFINITY));

    //. x = 1 ; y = NaN -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(1.0, NAN));


    //. -- x > 1 --
    //. x > 1 ; y = -Inf -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(12.3, -INFINITY));

    //. x > 1 ; y e {2k + 1; k e Z<0} -> x^y
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(12.3, -INFINITY));

    //. x > 1 ; y e {2k; k e Z<0} -> x^y
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(12.3, -INFINITY));

    //. x > 1 ; y < 0 ; y /e Z -> x^y
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(12.3, -INFINITY));

    //. x > 1 ; y = -0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(12.3, -0.0));

    //. x > 1 ; y = +0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(12.3, 0.0));

    //. x > 1 ; y > 0 ; y /e Z -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.f27aeded30940p+446, pow(12.3, 123.45));

    //. x > 1 ; y e {2k; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.5cfd21f9019d4p+43, pow(12.3, 12.0));

    //. x > 1 ; y e {2k - 1; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_DOUBLE(0x1.356f13f123d20p+83, pow(12.3, 23.0));

    //. x > 1 ; y = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(12.3, INFINITY));

    //. x > 1 ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(12.3, NAN));


    //. -- x = +Inf --
    //. x = +Inf ; y = -Inf -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(INFINITY, -INFINITY));

    //. x = +Inf ; y e {2k + 1; k e Z<0} -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(INFINITY, -INFINITY));

    //. x = +Inf ; y e {2k; k e Z<0} -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(INFINITY, -INFINITY));

    //. x = +Inf ; y < 0 ; y /e Z -> +0
    TEST_ASSERT_DOUBLE_IS_ZERO(pow(INFINITY, -INFINITY));

    //. x = +Inf ; y = -0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(INFINITY, -0.0));

    //. x = +Inf ; y = +0 -> +1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(INFINITY, 0.0));

    //. x = +Inf ; y > 0 ; y /e Z -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(INFINITY, 123.45));

    //. x = +Inf ; y e {2k; k e Z>0} -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(INFINITY, 12.0));

    //. x = +Inf ; y e {2k - 1; k e Z>0} -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(INFINITY, 23.0));

    //. x = +Inf ; y = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(pow(INFINITY, INFINITY));

    //. x = +Inf ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(INFINITY, NAN));


    //. -- x = NaN --
    //. x = NaN ; y = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(NAN, -INFINITY));

    //. x = NaN ; y e {2k + 1; k e Z<0} -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(NAN, -1.0));

    //. x = NaN ; y e {2k; k e Z<0} -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(NAN, -2.0));

    //. x = NaN ; y < 0 ; y /e Z  -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(NAN, -34.5));

    //. x = NaN ; y = -0  -> 1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(NAN, -0.0));

    //. x = NaN ; y = +0 -> 1
    TEST_ASSERT_EQUAL_DOUBLE(1.0, pow(NAN, 0.0));

    //. x = NaN ; y > 0 ; y /e Z -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(NAN, 1.23));

    //. x = NaN ; y e {2k; k e Z>0} -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(NAN, 4.0));

    //. x = NaN ; y e {2k - 1; k e Z>0} -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(NAN, 5.0));

    //. x = NaN ; y = +Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(NAN, INFINITY));

    //. x = NaN ; y = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(pow(NAN, NAN));
}


void test_math_double_special_rem2pi(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(rem2pi(-INFINITY));

    //. x < 0 -> rem2pi(x)
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.e95b14ce90ac0p-2, rem2pi(-123.0));

    //. x = -0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(rem2pi(-0.0));

    //. x = +0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(rem2pi(0.0));

    //. x > 0 -> rem2pi(x)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.4deed7eef9128p+0, rem2pi(12.3));

    //. x = +Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(rem2pi(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(rem2pi(NAN));
}

void test_math_double_special_round(void) {
    //. x = -Inf -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(round(-INFINITY));

    //. x < 0 -> ceil(x - 0.5)
    TEST_ASSERT_EQUAL_DOUBLE(-123.0, round(-123.45));
    TEST_ASSERT_EQUAL_DOUBLE(-124.0, round(-123.54));

    //. x = -0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(round(-0.0));

    //. x = +0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(round(0.0));

    //. x > 0 -> floor(x + 0.5)
    TEST_ASSERT_EQUAL_DOUBLE(12.0, round(12.345));
    TEST_ASSERT_EQUAL_DOUBLE(13.0, round(12.543));

    //. x = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(round(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(round(NAN));
}

void test_math_double_special_sin(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(sin(-INFINITY));

    //. x < 0 -> sin(x)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.99c8152fdc488p-1, sin(-123.45));

    //. x = -0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(sin(-0.0));

    //. x = +0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(sin(0.0));

    //. x > 0 -> sin(x)
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.cba85cb1eb4b9p-3, sin(12.34));

    //. x = +Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(sin(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(sin(NAN));
}

void test_math_double_special_sqrt(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(sqrt(-INFINITY));

    //. x < 0 -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(sqrt(-123.45));

    //. x = -0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(sqrt(-0.0));

    //. x = +0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(sqrt(0.0));

    //. x > 0 -> sqrt(x)
    TEST_ASSERT_EQUAL_DOUBLE(0x1.c1a4882851370p1, sqrt(12.34));

    //. x = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(sqrt(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(sqrt(NAN));
}

void test_math_double_special_tan(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(tan(-INFINITY));

    //. x < 0 -> tan(x)
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.55c10f487271cp0, tan(-123.45));

    //. x = -0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(tan(-0.0));

    //. x = +0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(tan(0.0));

    //. x > 0 -> tan(x)
    TEST_ASSERT_EQUAL_DOUBLE(-0x1.d7b11663a643fp-3, tan(12.34));

    //. x = +Inf -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(tan(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(tan(NAN));
}

void test_math_double_special_trunc(void) {
    //. x = -Inf -> -Inf
    TEST_ASSERT_DOUBLE_IS_NEG_INF(trunc(-INFINITY));

    //. x < 0 -> ceil(x)
    TEST_ASSERT_EQUAL_DOUBLE(-123.0, trunc(-123.45));
    TEST_ASSERT_EQUAL_DOUBLE(-12.0, trunc(-12.0));

    //. x = -0 -> x
    TEST_ASSERT_DOUBLE_IS_NEG_ZERO(trunc(-0.0));

    //. x = +0 -> x
    TEST_ASSERT_DOUBLE_IS_ZERO(trunc(0.0));

    //. x > 0 -> floor(x)
    TEST_ASSERT_EQUAL_DOUBLE(12.0, trunc(12.34));
    TEST_ASSERT_EQUAL_DOUBLE(123.0, trunc(123.0));

    //. x = +Inf -> +Inf
    TEST_ASSERT_DOUBLE_IS_INF(trunc(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(trunc(NAN));
}

void test_math_double_special_isfinite(void) {
    //. x = -Inf -> 0
    TEST_ASSERT_EQUAL_INT(0, isfinite(-INFINITY));

    //. x e R -> /0
    TEST_ASSERT_NOT_EQUAL(0, isfinite(-45.6));
    TEST_ASSERT_NOT_EQUAL(0, isfinite(-0.0));
    TEST_ASSERT_NOT_EQUAL(0, isfinite(0.0));
    TEST_ASSERT_NOT_EQUAL(0, isfinite(0.123));

    //. x = +Inf -> 0
    TEST_ASSERT_EQUAL_INT(0, isfinite(INFINITY));

    //. x = NaN -> 0
    TEST_ASSERT_EQUAL_INT(0, isfinite(NAN));
}

void test_math_double_special_isinf(void) {
    //. x = -Inf -> /0
    TEST_ASSERT_NOT_EQUAL(0, isinf(-INFINITY));

    //. x e R -> 0
    TEST_ASSERT_EQUAL_INT(0, isinf(-45.6));
    TEST_ASSERT_EQUAL_INT(0, isinf(-0.0));
    TEST_ASSERT_EQUAL_INT(0, isinf(0.0));
    TEST_ASSERT_EQUAL_INT(0, isinf(0.123));

    //. x = +Inf -> /0
    TEST_ASSERT_NOT_EQUAL(0, isinf(INFINITY));

    //. x = NaN -> 0
    TEST_ASSERT_EQUAL_INT(0, isinf(NAN));
}

void test_math_double_special_isnan(void) {
    //. x = -Inf -> 0
    TEST_ASSERT_EQUAL_INT(0, isnan(-INFINITY));

    //. x e R -> 0
    TEST_ASSERT_EQUAL_INT(0, isnan(-45.6));
    TEST_ASSERT_EQUAL_INT(0, isnan(-0.0));
    TEST_ASSERT_EQUAL_INT(0, isnan(0.0));
    TEST_ASSERT_EQUAL_INT(0, isnan(0.123));

    //. x = +Inf -> 0
    TEST_ASSERT_EQUAL_INT(0, isnan(INFINITY));

    //. x = NaN -> /0
    TEST_ASSERT_NOT_EQUAL(0, isnan(NAN));
}

void test_math_double_special_signbit(void) {
    //. x = -NaN -> /0
    TEST_ASSERT_NOT_EQUAL(0, signbit(-NAN));

    //. x = -Inf -> /0
    TEST_ASSERT_NOT_EQUAL(0, signbit(-INFINITY));

    //. x < 0 -> /0
    TEST_ASSERT_NOT_EQUAL(0, signbit(-123.0));

    //. x = -0 -> /0
    TEST_ASSERT_NOT_EQUAL(0, signbit(-0.0));

    //. x = +0 -> 0
    TEST_ASSERT_EQUAL_INT(0, signbit(0.0));

    //. x > 0 -> 0
    TEST_ASSERT_EQUAL_INT(0, signbit(34.5));

    //. x = +Inf -> 0
    TEST_ASSERT_EQUAL_INT(0, signbit(INFINITY));

    //. x = +NaN -> 0
    TEST_ASSERT_EQUAL_INT(0, signbit(NAN));
}

void test_math_double_special_fpclassify(void) {
    //. x = -Inf -> FP_INFINITE
    TEST_ASSERT_EQUAL(FP_INFINITE, fpclassify(-INFINITY));

    //. x < 0 -> FP_NORMAL
    TEST_ASSERT_EQUAL(FP_NORMAL, fpclassify(-123.0));

    //. x e -Denormal -> FP_SUBNORMAL
    TEST_ASSERT_EQUAL(FP_SUBNORMAL, fpclassify(-0x1.0p-1074));

    //. x = -0 -> FP_ZERO
    TEST_ASSERT_EQUAL(FP_ZERO, fpclassify(-0.0));

    //. x = +0 -> FP_ZERO
    TEST_ASSERT_EQUAL(FP_ZERO, fpclassify(0.0));

    //. x e -Denormal -> FP_SUBNORMAL
    TEST_ASSERT_EQUAL(FP_SUBNORMAL, fpclassify(0x1.0p-1074));

    //. x > 0 -> FP_NORMAL
    TEST_ASSERT_EQUAL(FP_NORMAL, fpclassify(34.5));

    //. x = +Inf -> FP_INFINITE
    TEST_ASSERT_EQUAL(FP_INFINITE, fpclassify(INFINITY));

    //. x = NaN -> FP_NAN
    TEST_ASSERT_EQUAL(FP_NAN, fpclassify(NAN));
}

void test_math_double_special_nan(void) {
    //. -> NaN
    TEST_ASSERT_DOUBLE_IS_NAN(nan(""));
}

int main(void) {
    UNITY_BEGIN();

    RUN_TEST(test_math_double_special_acos);
    RUN_TEST(test_math_double_special_asin);
    RUN_TEST(test_math_double_special_atan);
    RUN_TEST(test_math_double_special_atan2);
    RUN_TEST(test_math_double_special_ceil);
    RUN_TEST(test_math_double_special_copysign);
    RUN_TEST(test_math_double_special_cos);
    RUN_TEST(test_math_double_special_deg2rad);
    RUN_TEST(test_math_double_special_exp);
    RUN_TEST(test_math_double_special_fabs);
    RUN_TEST(test_math_double_special_floor);
    RUN_TEST(test_math_double_special_fmax);
    RUN_TEST(test_math_double_special_fmin);
    RUN_TEST(test_math_double_special_fmod);
    RUN_TEST(test_math_double_special_hypot);
    RUN_TEST(test_math_double_special_log);
    RUN_TEST(test_math_double_special_log10);
    RUN_TEST(test_math_double_special_modf);
    RUN_TEST(test_math_double_special_pow);
    RUN_TEST(test_math_double_special_rem2pi);
    RUN_TEST(test_math_double_special_round);
    RUN_TEST(test_math_double_special_sin);
    RUN_TEST(test_math_double_special_sqrt);
    RUN_TEST(test_math_double_special_tan);
    RUN_TEST(test_math_double_special_trunc);
    RUN_TEST(test_math_double_special_isfinite);
    RUN_TEST(test_math_double_special_isinf);
    RUN_TEST(test_math_double_special_isnan);
    RUN_TEST(test_math_double_special_signbit);
    RUN_TEST(test_math_double_special_fpclassify);
    RUN_TEST(test_math_double_special_nan);

    return UNITY_END();
}
