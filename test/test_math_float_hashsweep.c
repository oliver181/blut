#include "unity.h"

#include <float.h>
#include <math.h>
#include <stdio.h>

#define XXH_INLINE_ALL
#include "xxhash.h"

float deg2radf(float x) {
    if (x == -0.0f && signbit(x) != 0) return -0.0f;
    return x * (M_PI/180.0f);
}

float rem2pif(float x) {
    double dx = x;
    return (float)fmod(dx, M_PI_2);
}

#include "test_math_float_hashtab_glibc-2.27.inc"

typedef float (*math_one_arg_func_t)(float x);

union float_u32 {
    float f;
    uint32_t u32;
};

static void test_math_float_hashsweep(math_one_arg_func_t func, const uint32_t * hash_tab) {
    XXH32_state_t * state = XXH32_createState();

    for (uint32_t block = 0x0; block <= 0xF; block += 1U) {
        // hash seed = block number
        XXH32_reset(state, block);
        for (uint32_t index = 0x00000000; index <= 0x0FFFFFFF; index += 1U) {
            union float_u32 u = { .u32 = ((block << 28U) | index) };

            u.f = func(u.f);

            XXH32_update(state, &(u.u32), sizeof u.u32);
        }

        uint32_t hash = XXH32_digest(state);

        printf("INFO: Block: %02u/15, Expected: 0x%08xU, Actual: 0x%08xU,\n",
               block, hash_tab[block], hash);
        TEST_ASSERT_EQUAL_HEX32(hash_tab[block], hash);
    }
}

void test_math_float_hashsweep_acosf(void) {
    test_math_float_hashsweep(acosf, hash_acosf_tab);
}

void test_math_float_hashsweep_asinf(void) {
    test_math_float_hashsweep(asinf, hash_asinf_tab);
}

void test_math_float_hashsweep_atanf(void) {
    test_math_float_hashsweep(atanf, hash_atanf_tab);
}

void test_math_float_hashsweep_ceilf(void) {
    test_math_float_hashsweep(ceilf, hash_ceilf_tab);
}

void test_math_float_hashsweep_cosf(void) {
    test_math_float_hashsweep(cosf, hash_cosf_tab);
}

void test_math_float_hashsweep_deg2radf(void) {
    test_math_float_hashsweep(deg2radf, hash_deg2radf_tab);
}

void test_math_float_hashsweep_expf(void) {
    test_math_float_hashsweep(expf, hash_expf_tab);
}

void test_math_float_hashsweep_fabsf(void) {
    test_math_float_hashsweep(fabsf, hash_fabsf_tab);
}

void test_math_float_hashsweep_floorf(void) {
    test_math_float_hashsweep(floorf, hash_floorf_tab);
}

void test_math_float_hashsweep_logf(void) {
    test_math_float_hashsweep(logf, hash_logf_tab);
}

void test_math_float_hashsweep_log10f(void) {
    test_math_float_hashsweep(log10f, hash_log10f_tab);
}

void test_math_float_hashsweep_rem2pif(void) {
    test_math_float_hashsweep(rem2pif, hash_rem2pif_tab);
}

void test_math_float_hashsweep_roundf(void) {
    test_math_float_hashsweep(roundf, hash_roundf_tab);
}

void test_math_float_hashsweep_sinf(void) {
    test_math_float_hashsweep(sinf, hash_sinf_tab);
}

void test_math_float_hashsweep_sqrtf(void) {
    test_math_float_hashsweep(sqrtf, hash_sqrtf_tab);
}

void test_math_float_hashsweep_tanf(void) {
    test_math_float_hashsweep(tanf, hash_tanf_tab);
}

void test_math_float_hashsweep_truncf(void) {
    test_math_float_hashsweep(truncf, hash_truncf_tab);
}


int main(void) {
    UNITY_BEGIN();

    RUN_TEST(test_math_float_hashsweep_acosf);
    RUN_TEST(test_math_float_hashsweep_asinf);
    RUN_TEST(test_math_float_hashsweep_atanf);
    RUN_TEST(test_math_float_hashsweep_ceilf);
    RUN_TEST(test_math_float_hashsweep_cosf);
    RUN_TEST(test_math_float_hashsweep_deg2radf);
    RUN_TEST(test_math_float_hashsweep_expf);
    RUN_TEST(test_math_float_hashsweep_fabsf);
    RUN_TEST(test_math_float_hashsweep_floorf);
    RUN_TEST(test_math_float_hashsweep_logf);
    RUN_TEST(test_math_float_hashsweep_log10f);
    RUN_TEST(test_math_float_hashsweep_rem2pif);
    RUN_TEST(test_math_float_hashsweep_roundf);
    RUN_TEST(test_math_float_hashsweep_sinf);
    RUN_TEST(test_math_float_hashsweep_sqrtf);
    RUN_TEST(test_math_float_hashsweep_tanf);
    RUN_TEST(test_math_float_hashsweep_truncf);

    return UNITY_END();
}