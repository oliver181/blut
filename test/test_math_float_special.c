#include "unity.h"

#include <math.h>

#define TEST_ASSERT_FLOAT_IS_NEG_ZERO(actual) TEST_ASSERT_MESSAGE((0.0f == (actual)) && (0 != signbit(actual)), "Expected -0.0f")
#define TEST_ASSERT_FLOAT_IS_ZERO(actual) TEST_ASSERT_MESSAGE((0.0f == (actual)) && (0 == signbit(actual)), "Expected 0.0f")

float deg2radf(float x) {
    if (x == -0.0f && signbit(x) != 0) return -0.0f;
    return (float)(x * (M_PI/180.0f));
}

float rem2pif(float x) {
    double dx = x;
    return (float)fmod(dx, M_PI_2);
}

void test_math_float_special_acosf(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(acosf(-INFINITY));

    //. x < -1 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(acosf(-12.3f));

    //. x in [-1; +1[ -> acos(x)
    TEST_ASSERT_EQUAL_FLOAT(0x1.359d26f9p1f, acosf(-0.75f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.0c152382p1f, acosf(-0.5f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.921fb544p0f, acosf(0.0f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.51700e0cp0f, acosf(0.25f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.dac67056p-1f, acosf(0.6f));

    //. x = +1 -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(acosf(1.0f));

    //. x > +1 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(acosf(12.3f));

    //. x = +Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(acosf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(acosf(NAN));
}

void test_math_float_special_asinf(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(asinf(-INFINITY));

    //. x < -1 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(asinf(-12.3f));

    //. x in [-1; -0[ -> asin(x)
    TEST_ASSERT_EQUAL_FLOAT(-0x1.b235315cp-1f, asinf(-0.75f));
    TEST_ASSERT_EQUAL_FLOAT(-0x1.0c152382p-1f, asinf(-0.5f));
    TEST_ASSERT_EQUAL_FLOAT(-0x1.9a492760p-4f, asinf(-0.1f));

    //. x = -0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(asinf(-0.0f));

    //. x = +0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(asinf(0.0f));

    //. x in ]+0; +1] -> asin(x)
    TEST_ASSERT_EQUAL_FLOAT(0x1.9a492760p-4f, asinf(0.1f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.02be9ce0p-2f, asinf(0.25f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.4978fb32p-1f, asinf(0.6f));

    //. x > +1 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(asinf(12.3f));

    //. x = +Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(asinf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(asinf(NAN));
}

void test_math_float_special_atanf(void) {
    //. x = -Inf -> -π/2
    TEST_ASSERT_EQUAL_FLOAT(-M_PI_2, atanf(-INFINITY));

    //. x < 0 -> atan(x)
    TEST_ASSERT_EQUAL_FLOAT(-0x1.900ae824p0f, atanf(-123.0f));
    TEST_ASSERT_EQUAL_FLOAT(-0x1.7d5b455fp0f, atanf(-12.3f));
    TEST_ASSERT_EQUAL_FLOAT(-0x1.f54a45bcp-4f, atanf(-0.123f));

    //. x = -0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(atanf(-0.0f));

    //. x = +0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(atanf(0.0f));

    //. x > 0 -> atan(x)
    TEST_ASSERT_EQUAL_FLOAT(0x1.47816237p-5f, atanf(0.04f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.b1009e9ep-2f, atanf(0.45f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.5adbbda1p0f, atanf(4.56f));

    //. x = +Inf -> +π/2
    TEST_ASSERT_EQUAL_FLOAT(M_PI_2, atanf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atanf(NAN));
}

void test_math_float_special_atan2f(void) {
    //. -- y = -Inf --
    //. y = -Inf; x = -Inf -> -3/4π
    TEST_ASSERT_EQUAL_FLOAT(-3.0f * M_PI_4, atan2f(-INFINITY, -INFINITY));

    //. y = -Inf; x < 0 -> -π/2
    TEST_ASSERT_EQUAL_FLOAT(-M_PI_2, atan2f(-INFINITY, -123.0f));

    //. y = -Inf; x = -0 -> -π/2
    TEST_ASSERT_EQUAL_FLOAT(-M_PI_2, atan2f(-INFINITY, -0.0f));

    //. y = -Inf; x = +0 -> -π/2
    TEST_ASSERT_EQUAL_FLOAT(-M_PI_2, atan2f(-INFINITY, 0.0f));

    //. y = -Inf; x > 0 -> -π/2
    TEST_ASSERT_EQUAL_FLOAT(-M_PI_2, atan2f(-INFINITY, 123.0f));

    //. y = -Inf; x = +Inf -> -π/4
    TEST_ASSERT_EQUAL_FLOAT(-M_PI_4, atan2f(-INFINITY, INFINITY));

    //. y = -Inf; x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(-INFINITY, NAN));


    //. -- y < 0 --
    //. y < 0; x = -Inf -> -π
    TEST_ASSERT_EQUAL_FLOAT(-M_PI, atan2f(-123.0f, -INFINITY));

    //. y < 0; x < 0 -> atan(y/x)
    TEST_ASSERT_EQUAL_FLOAT(-0x1.2d97c7f3p1, atan2f(-123.0f, -123.0f));

    //. y < 0; x = -0 -> -π/2
    TEST_ASSERT_EQUAL_FLOAT(-M_PI_2, atan2f(-123.0f, -0.0f));

    //. y < 0; x = +0 -> -π/2
    TEST_ASSERT_EQUAL_FLOAT(-M_PI_2, atan2f(-123.0f, 0.0f));

    //. y < 0; x > 0 -> atan(y/x)
    TEST_ASSERT_EQUAL_FLOAT(-M_PI, atan2f(-123.0f, -INFINITY));

    //. y < 0; x = +Inf -> -0
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(atan2f(-123.0f, INFINITY));

    //. y < 0; x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(-123.0f, NAN));


    //. -- y = -0 --
    //. y = -0; x = -Inf -> -π
    TEST_ASSERT_EQUAL_FLOAT(-M_PI, atan2f(-0.0f, -INFINITY));

    //. y = -0; x < 0 -> -π
    TEST_ASSERT_EQUAL_FLOAT(-M_PI, atan2f(-0.0f, -123.0f));

    //. y = -0; x = -0 -> -π
    TEST_ASSERT_EQUAL_FLOAT(-M_PI, atan2f(-0.0f, -0.0f));

    //. y = -0; x = +0 -> -0
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(atan2f(-0.0f, 0.0f));

    //. y = -0; x > 0 -> -0
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(atan2f(-0.0f, 123.0f));

    //. y = -0; x = +Inf -> -0
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(atan2f(-0.0f, INFINITY));

    //. y = -0; x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(-0.0f, NAN));


    //. -- y = +0 --
    //. y = +0; x = -Inf -> +π
    TEST_ASSERT_EQUAL_FLOAT(M_PI, atan2f(0.0f, -INFINITY));

    //. y = +0; x < 0 -> +π
    TEST_ASSERT_EQUAL_FLOAT(M_PI, atan2f(0.0f, -123.0f));

    //. y = +0; x = -0 -> +π
    TEST_ASSERT_EQUAL_FLOAT(M_PI, atan2f(0.0f, -0.0f));

    //. y = +0; x = +0 -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(atan2f(0.0f, 0.0f));

    //. y = +0; x > 0 -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(atan2f(0.0f, 123.0f));

    //. y = +0; x = +Inf -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(atan2f(0.0f, INFINITY));

    //. y = +0; x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(0.0f, NAN));


    //. -- y > 0 --
    //. y > 0; x = -Inf -> +π
    TEST_ASSERT_EQUAL_FLOAT(M_PI, atan2f(123.0f, -INFINITY));

    //. y > 0; x < 0 -> atan(y/x)
    TEST_ASSERT_EQUAL_FLOAT(0x1.2d97c7f3p1, atan2f(123.0f, -123.0f));

    //. y > 0; x = -0 -> +π/2
    TEST_ASSERT_EQUAL_FLOAT(M_PI_2, atan2f(123.0f, -0.0f));

    //. y > 0; x = +0 -> +π/2
    TEST_ASSERT_EQUAL_FLOAT(M_PI_2, atan2f(123.0f, 0.0f));

    //. y > 0; x > 0 -> atan(y/x)
    TEST_ASSERT_EQUAL_FLOAT(0x1.921fb544p-1f, atan2f(123.0f, 123.0f));

    //. y > 0; x = +Inf -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(atan2f(123.0f, INFINITY));

    //. y > 0; x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(123.0f, NAN));


    //. -- y = +Inf --
    //. y = +Inf; x = -Inf -> 3/4π
    TEST_ASSERT_EQUAL_FLOAT(3.0f * M_PI_4, atan2f(INFINITY, -INFINITY));

    //. y = +Inf; x < 0 -> +π/2
    TEST_ASSERT_EQUAL_FLOAT(M_PI_2, atan2f(INFINITY, -123.0f));

    //. y = +Inf; x = -0 -> +π/2
    TEST_ASSERT_EQUAL_FLOAT(M_PI_2, atan2f(INFINITY, -0.0f));

    //. y = +Inf; x = +0 -> +π/2
    TEST_ASSERT_EQUAL_FLOAT(M_PI_2, atan2f(INFINITY, 0.0f));

    //. y = +Inf; x > 0 -> +π/2
    TEST_ASSERT_EQUAL_FLOAT(M_PI_2, atan2f(INFINITY, 123.0f));

    //. y = +Inf; x = +Inf -> π/4
    TEST_ASSERT_EQUAL_FLOAT(M_PI_4, atan2f(INFINITY, INFINITY));

    //. y = +Inf; x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(INFINITY, NAN));


    //. -- y = NaN --
    //. y = NaN; x = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(NAN, -INFINITY));

    //. y = NaN; x < 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(NAN, -123.0f));

    //. y = NaN; x = -0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(NAN, -0.0f));

    //. y = NaN; x = +0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(NAN, 0.0f));

    //. y = NaN; x > 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(NAN, 123.0f));

    //. y = NaN; x = +Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(NAN, INFINITY));

    //. y = NaN; x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(atan2f(NAN, NAN));
}

void test_math_float_special_ceilf(void) {
    //. x = -Inf -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(ceilf(-INFINITY));

    //. x < 0 -> ceil(x)
    TEST_ASSERT_EQUAL_FLOAT(-123.0f, ceilf(-123.456f));
    TEST_ASSERT_EQUAL_FLOAT(-12.0f, ceilf(-12.3f));
    TEST_ASSERT_EQUAL_FLOAT(-1.0f, ceilf(-1.23456f));

    //. x = -0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(ceilf(-0.0f));

    //. x = +0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(ceilf(0.0f));

    //. x > 0 -> ceil(x)
    TEST_ASSERT_EQUAL_FLOAT(2.0f, ceilf(1.23456f));
    TEST_ASSERT_EQUAL_FLOAT(13.0f, ceilf(12.3f));
    TEST_ASSERT_EQUAL_FLOAT(124.0f, ceilf(123.456f));

    //. x = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(ceilf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(ceilf(NAN));
}

void test_math_float_special_copysignf(void) {
    //. -- x /= NaN --
    //. x /= NaN ; y = -NaN -> -|x|
    TEST_ASSERT_EQUAL_FLOAT(-1.23456f, copysignf(1.23456f, -NAN));

    //. x /= NaN ; y = -Inf -> -|x|
    TEST_ASSERT_EQUAL_FLOAT(-2.34561f, copysignf(-2.34561f, -INFINITY));

    //. x /= NaN ; y < 0 -> -|x|
    TEST_ASSERT_EQUAL_FLOAT(-3.45612f, copysignf(3.45612f, -23.0f));

    //. x /= NaN ; y = -0 -> -|x|
    TEST_ASSERT_EQUAL_FLOAT(-4.56123f, copysignf(-4.56123f, -0.0f));

    //. x /= NaN ; y = +0 -> |x|
    TEST_ASSERT_EQUAL_FLOAT(4.56123f, copysignf(-4.56123f, 0.0f));

    //. x /= NaN ; y > 0 -> |x|
    TEST_ASSERT_EQUAL_FLOAT(3.45612f, copysignf(3.45612f, 123.0f));

    //. x /= NaN ; y = +Inf -> |x|
    TEST_ASSERT_EQUAL_FLOAT(2.34561f, copysignf(-2.34561f, INFINITY));

    //. x /= NaN ; y = +NaN -> |x|
    TEST_ASSERT_EQUAL_FLOAT(1.23456f, copysignf(1.23456f, NAN));


    //. -- x = NaN --
    //. x = NaN ; y = -NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(copysignf(NAN, -NAN));

    //. x = NaN ; y = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(copysignf(NAN, -INFINITY));

    //. x = NaN ; y < 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(copysignf(NAN, -123.0f));

    //. x = NaN ; y = -0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(copysignf(NAN, -0.0f));

    //. x = NaN ; y = +0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(copysignf(NAN, 0.0f));

    //. x = NaN ; y > 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(copysignf(NAN, 23.0f));

    //. x = NaN ; y = +Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(copysignf(NAN, INFINITY));

    //. x = NaN ; y = +NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(copysignf(NAN, NAN));
}

void test_math_float_special_cosf(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(cosf(-INFINITY));

    //. x < 0 -> cos(x)
    TEST_ASSERT_EQUAL_FLOAT(-0x1.c6a3dc4cp-1, cosf(-123.0f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.edf16f06p-1, cosf(-12.3f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.564268f7p-2, cosf(-1.23f));

    //. x = -0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, cosf(-0.0f));

    //. x = +0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, cosf(0.0f));

    //. x > 0 -> cos(x)
    TEST_ASSERT_EQUAL_FLOAT(0x1.fc21c154p-1, cosf(0.123f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.564268f7p-2, cosf(1.23f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.edf16f06p-1, cosf(12.3f));

    //. x = +Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(cosf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(cosf(NAN));
}

void test_math_float_special_deg2radf(void) {
    //. x = -Inf -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(deg2radf(-INFINITY));

    //. x < 0 -> x in rad
    TEST_ASSERT_EQUAL_FLOAT(-0x1.12c8ddffp1, deg2radf(-123.0f));
    TEST_ASSERT_EQUAL_FLOAT(-0x1.b7a7ca0bp-3, deg2radf(-12.3f));
    TEST_ASSERT_EQUAL_FLOAT(-0x1.5fb96e14p-6, deg2radf(-1.23f));

    //. x = -0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(deg2radf(-0.0f));

    //. x = +0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(deg2radf(0.0f));

    //. x > 0 -> x in rad
    TEST_ASSERT_EQUAL_FLOAT(0x1.1961255fp-9, deg2radf(0.123f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.5fb96e14p-6, deg2radf(1.23f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.b7a7c999p-3, deg2radf(12.3f));

    //. x = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(deg2radf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(deg2radf(NAN));
}

void test_math_float_special_expf(void) {
    //. x = -Inf -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(expf(-INFINITY));

    //. x < 0 -> e^x
    TEST_ASSERT_EQUAL_FLOAT(0x1.766b45ddp-178, expf(-123.0f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.31765c09p-18, expf(-12.3f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.2b4ebed8p-2, expf(-1.23f));

    //. x = -0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, expf(-0.0f));

    //. x = +0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, expf(0.0f));

    //. x > 0 -> e^x
    TEST_ASSERT_EQUAL_FLOAT(0x1.2181a433p0, expf(0.123f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.b5ead975p1, expf(1.23f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.ad1803e9p17, expf(12.3f));

    //. x = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(expf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(expf(NAN));
}

void test_math_float_special_fabsf(void) {
    //. x = -Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(fabsf(-INFINITY));

    //. x < 0 -> |x|
    TEST_ASSERT_EQUAL_FLOAT(123.0f, fabsf(-123.0f));
    TEST_ASSERT_EQUAL_FLOAT(12.3f, fabsf(-12.3f));
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fabsf(-1.23f));

    //. x = -0 -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(fabsf(-0.0f));

    //. x = +0 -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(fabsf(0.0f));

    //. x > 0 -> x
    TEST_ASSERT_EQUAL_FLOAT(0.123f, fabsf(0.123f));
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fabsf(1.23f));
    TEST_ASSERT_EQUAL_FLOAT(12.3f, fabsf(12.3f));

    //. x = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(fabsf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fabsf(NAN));
}

void test_math_float_special_floorf(void) {
    //. x = -Inf -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(floorf(-INFINITY));

    //. x < 0 -> floor(x)
    TEST_ASSERT_EQUAL_FLOAT(-123.0f, floorf(-123.0f));
    TEST_ASSERT_EQUAL_FLOAT(-13.0f, floorf(-12.3f));
    TEST_ASSERT_EQUAL_FLOAT(-2.00f, floorf(-1.23f));

    //. x = -0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(floorf(-0.0f));

    //. x = +0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(floorf(0.0f));

    //. x > 0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(floorf(0.123f));
    TEST_ASSERT_EQUAL_FLOAT(1.0f, floorf(1.23f));
    TEST_ASSERT_EQUAL_FLOAT(12.0f, floorf(12.3f));

    //. x = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(floorf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(floorf(NAN));
}

void test_math_float_special_fmaxf(void) {
    //. -- x = -Inf --
    //. x = -Inf ; y = -Inf -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(fmaxf(-INFINITY, -INFINITY));

    //. x = -Inf ; y < 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fmaxf(-INFINITY, -12.3f));

    //. x = -Inf ; y = -0 -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmaxf(-INFINITY, -0.0f));

    //. x = -Inf ; y = +0 -> y
    TEST_ASSERT_FLOAT_IS_ZERO(fmaxf(-INFINITY, 0.0f));

    //. x = -Inf ; y > 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(12.3f, fmaxf(-INFINITY, 12.3f));

    //. x = -Inf ; y = +Inf -> y
    TEST_ASSERT_FLOAT_IS_INF(fmaxf(-INFINITY, INFINITY));

    //. x = -Inf ; y = NaN -> x
    TEST_ASSERT_FLOAT_IS_NEG_INF(fmaxf(-INFINITY, NAN));


    //. -- x < 0 --
    //. x < 0 ; y = -Inf -> x
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fmaxf(-12.3f, -INFINITY));

    //. x < 0 ; y < 0 -> max(x, y)
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fmaxf(-12.3f, -123.0f));
    TEST_ASSERT_EQUAL_FLOAT(-1.23f, fmaxf(-12.3f, -1.23f));

    //. x < 0 ; y = -0 -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmaxf(-12.3f, -0.0f));

    //. x < 0 ; y = +0 -> y
    TEST_ASSERT_FLOAT_IS_ZERO(fmaxf(-12.3f, 0.0f));

    //. x < 0 ; y > 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(3.45f, fmaxf(-12.3f, 3.45f));

    //. x < 0 ; y = +Inf -> y
    TEST_ASSERT_EQUAL_FLOAT(INFINITY, fmaxf(-12.3f, INFINITY));

    //. x < 0 ; y = NaN -> x
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fmaxf(-12.3f, NAN));


    //. -- x = -0 --
    //. x = -0 ; y = -Inf -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmaxf(-0.0f, -INFINITY));

    //. x = -0 ; y < 0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmaxf(-0.0f, -12.3f));

    //. x = -0 ; y = -0 -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmaxf(-0.0f, -INFINITY));

    //. x = -0 ; y = +0 -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmaxf(-0.0f, -INFINITY));

    //. x = -0 ; y > 0 -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmaxf(-0.0f, -INFINITY));

    //. x = -0 ; y = +Inf -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmaxf(-0.0f, -INFINITY));

    //. x = -0 ; y = NaN -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmaxf(-0.0f, -INFINITY));


    //. -- x = +0 --
    //. x = +0 ; y = -Inf -> x
    TEST_ASSERT_FLOAT_IS_ZERO(fmaxf(0.0f, -INFINITY));

    //. x = +0 ; y < 0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(fmaxf(0.0f, -12.3f));

    //. x = +0 ; y = -0 -> y
    TEST_ASSERT_FLOAT_IS_ZERO(fmaxf(0.0f, -0.0f));

    //. x = +0 ; y = +0 -> y
    TEST_ASSERT_FLOAT_IS_ZERO(fmaxf(0.0f, 0.0f));

    //. x = +0 ; y > 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(12.3f, fmaxf(0.0f, 12.3f));

    //. x = +0 ; y = +Inf -> y
    TEST_ASSERT_EQUAL_FLOAT(INFINITY, fmaxf(0.0f, INFINITY));

    //. x = +0 ; y = NaN -> x
    TEST_ASSERT_FLOAT_IS_ZERO(fmaxf(0.0f, NAN));


    //. -- x > 0 --
    //. x > 0 ; y = -Inf -> x
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fmaxf(1.23f, NAN));

    //. x > 0 ; y < 0 -> x
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fmaxf(1.23f, -12.3f));

    //. x > 0 ; y = -0 -> x
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fmaxf(1.23f, -0.0f));

    //. x > 0 ; y = +0 -> x
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fmaxf(1.23f, 0.0f));

    //. x > 0 ; y > 0 -> max(x, y)
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fmaxf(1.23f, 0.123f));
    TEST_ASSERT_EQUAL_FLOAT(12.3f, fmaxf(1.23f, 12.3f));

    //. x > 0 ; y = +Inf -> y
    TEST_ASSERT_FLOAT_IS_INF(fmaxf(1.23f, INFINITY));

    //. x > 0 ; y = NaN -> x
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fmaxf(1.23f, NAN));


    //. -- x = +Inf --
    //. x = +Inf ; y = -Inf -> x
    TEST_ASSERT_FLOAT_IS_INF(fmaxf(INFINITY, -INFINITY));

    //. x = +Inf ; y < 0 -> x
    TEST_ASSERT_FLOAT_IS_INF(fmaxf(INFINITY, -12.3f));

    //. x = +Inf ; y = -0 -> x
    TEST_ASSERT_FLOAT_IS_INF(fmaxf(INFINITY, -0.0f));

    //. x = +Inf ; y = +0 -> x
    TEST_ASSERT_FLOAT_IS_INF(fmaxf(INFINITY, 0.0f));

    //. x = +Inf ; y > 0 -> x
    TEST_ASSERT_FLOAT_IS_INF(fmaxf(INFINITY, 12.3f));

    //. x = +Inf ; y = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(fmaxf(INFINITY, INFINITY));

    //. x = +Inf ; y = NaN -> x
    TEST_ASSERT_FLOAT_IS_INF(fmaxf(INFINITY, NAN));


    //. -- x = NaN --
    //. x = NaN ; y = -Inf -> y
    TEST_ASSERT_FLOAT_IS_NEG_INF(fmaxf(NAN, -INFINITY));

    //. x = NaN ; y < 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fmaxf(NAN, -12.3f));

    //. x = NaN ; y = -0 -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmaxf(NAN, -0.0f));

    //. x = NaN ; y = +0 -> y
    TEST_ASSERT_FLOAT_IS_ZERO(fmaxf(NAN, 0.0f));

    //. x = NaN ; y > 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(12.3f, fmaxf(NAN, 12.3f));

    //. x = NaN ; y = +Inf -> y
    TEST_ASSERT_FLOAT_IS_INF(fmaxf(NAN, INFINITY));

    //. x = NaN ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmaxf(NAN, NAN));
}

void test_math_float_special_fminf(void) {
    //. -- x = -Inf --
    //. x = -Inf ; y = -Inf -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(-INFINITY, -INFINITY));

    //. x = -Inf ; y < 0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(-INFINITY, -12.3f));

    //. x = -Inf ; y = -0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(-INFINITY, -0.0f));

    //. x = -Inf ; y = +0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(-INFINITY, 0.0f));

    //. x = -Inf ; y > 0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(-INFINITY, 12.3f));

    //. x = -Inf ; y = +Inf -> x
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(-INFINITY, INFINITY));

    //. x = -Inf ; y = NaN -> x
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(-INFINITY, NAN));


    //. -- x < 0 --
    //. x < 0 ; y = -Inf -> y
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(-12.3f, -INFINITY));

    //. x < 0 ; y < 0 -> min(x, y)
    TEST_ASSERT_EQUAL_FLOAT(-123.0f, fminf(-12.3f, -123.0f));
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fminf(-12.3f, -1.23f));

    //. x < 0 ; y = -0 -> x
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fminf(-12.3f, -0.0f));

    //. x < 0 ; y = +0 -> x
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fminf(-12.3f, 0.0f));

    //. x < 0 ; y > 0 -> x
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fminf(-12.3f, 3.45f));

    //. x < 0 ; y = +Inf -> x
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fminf(-12.3f, INFINITY));

    //. x < 0 ; y = NaN -> x
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fminf(-12.3f, NAN));


    //. -- x = -0 --
    //. x = -0 ; y = -Inf -> y
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(-0.0f, -INFINITY));

    //. x = -0 ; y < 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fminf(-0.0f, -12.3f));

    //. x = -0 ; y = -0 -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fminf(-0.0f, -0.0f));

    //. x = -0 ; y = +0 -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fminf(-0.0f, 0.0f));

    //. x = -0 ; y > 0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fminf(-0.0f, 12.3f));

    //. x = -0 ; y = +Inf -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fminf(-0.0f, INFINITY));

    //. x = -0 ; y = NaN -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fminf(-0.0f, NAN));


    //. -- x = +0 --
    //. x = +0 ; y = -Inf -> y
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(0.0f, -INFINITY));

    //. x = +0 ; y < 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fminf(0.0f, -12.3f));

    //. x = +0 ; y = -0 -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fminf(0.0f, -0.0f));

    //. x = +0 ; y = +0 -> y
    TEST_ASSERT_FLOAT_IS_ZERO(fminf(0.0f, 0.0f));

    //. x = +0 ; y > 0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(fminf(0.0f, 12.3f));

    //. x = +0 ; y = +Inf -> x
    TEST_ASSERT_FLOAT_IS_ZERO(fminf(0.0f, INFINITY));

    //. x = +0 ; y = NaN -> x
    TEST_ASSERT_FLOAT_IS_ZERO(fminf(0.0f, NAN));


    //. -- x > 0 --
    //. x > 0 ; y = -Inf -> y
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(1.23f, -INFINITY));

    //. x > 0 ; y < 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fminf(1.23f, -12.3f));

    //. x > 0 ; y = -0 -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fminf(1.23f, -0.0f));

    //. x > 0 ; y = +0 -> y
    TEST_ASSERT_FLOAT_IS_ZERO(fminf(1.23f, 0.0f));

    //. x > 0 ; y > 0 -> min(x, y)
    TEST_ASSERT_EQUAL_FLOAT(0.123f, fminf(1.23f, 0.123f));
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fminf(1.23f, 12.3f));

    //. x > 0 ; y = +Inf -> x
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fminf(1.23f, INFINITY));

    //. x > 0 ; y = NaN -> x
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fminf(1.23f, NAN));


    //. -- x = +Inf --
    //. x = +Inf ; y = -Inf -> y
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(INFINITY, -INFINITY));

    //. x = +Inf ; y < 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fminf(INFINITY, -12.3f));

    //. x = +Inf ; y = -0 -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fminf(INFINITY, -0.0f));

    //. x = +Inf ; y = +0 -> y
    TEST_ASSERT_FLOAT_IS_ZERO(fminf(INFINITY, 0.0f));

    //. x = +Inf ; y > 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(12.3f, fminf(INFINITY, 12.3f));

    //. x = +Inf ; y = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(fminf(INFINITY, INFINITY));

    //. x = +Inf ; y = NaN -> x
    TEST_ASSERT_FLOAT_IS_INF(fminf(INFINITY, NAN));


    //. -- x = NaN --
    //. x = NaN ; y = -Inf -> y
    TEST_ASSERT_FLOAT_IS_NEG_INF(fminf(NAN, -INFINITY));

    //. x = NaN ; y < 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fminf(NAN, -12.3f));

    //. x = NaN ; y = -0 -> y
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fminf(NAN, -0.0f));

    //. x = NaN ; y = +0 -> y
    TEST_ASSERT_FLOAT_IS_ZERO(fminf(NAN, 0.0f));

    //. x = NaN ; y > 0 -> y
    TEST_ASSERT_EQUAL_FLOAT(12.3f, fminf(NAN, 12.3f));

    //. x = NaN ; y = +Inf -> y
    TEST_ASSERT_FLOAT_IS_INF(fminf(NAN, INFINITY));

    //. x = NaN ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fminf(NAN, NAN));
}

void test_math_float_special_fmodf(void) {
    //. -- x = -Inf --
    //. x = -Inf ; y = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-INFINITY, -INFINITY));

    //. x = -Inf ; y < 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-INFINITY, -12.3f));

    //. x = -Inf ; y = -0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-INFINITY, -0.0f));

    //. x = -Inf ; y = +0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-INFINITY, 0.0f));

    //. x = -Inf ; y > 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-INFINITY, 12.3f));

    //. x = -Inf ; y = +Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-INFINITY, INFINITY));

    //. x = -Inf ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-INFINITY, NAN));


    //. -- x < 0 --
    //. x < 0 ; y = -Inf -> x
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fmodf(-12.3f, -INFINITY));

    //. x < 0 ; y < 0 -> fmod(x, y)
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fmodf(-12.3f, -123.0f));
    TEST_ASSERT_EQUAL_FLOAT(-0x1.89999600p3f, fmodf(-123.0f, -12.3f));

    //. x < 0 ; y = -0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-12.3f, -0.0f));

    //. x < 0 ; y = +0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-12.3f, 0.0f));

    //. x < 0 ; y > 0 -> fmod(x, y)
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fmodf(-12.3f, 123.0f));
    TEST_ASSERT_EQUAL_FLOAT(-0x1.89999600p3f, fmodf(-123.0f, 12.3f));

    //. x < 0 ; y = +Inf -> x
    TEST_ASSERT_EQUAL_FLOAT(-12.3f, fmodf(-12.3f, INFINITY));

    //. x < 0 ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-12.3f, NAN));


    //. -- x = -0 --
    //. x = -0 ; y = -Inf -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmodf(-0.0f, -INFINITY));

    //. x = -0 ; y < 0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmodf(-0.0f, -12.3f));

    //. x = -0 ; y = -0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-0.0f, -0.0f));

    //. x = -0 ; y = +0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-0.0f, 0.0f));

    //. x = -0 ; y > 0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmodf(-0.0f, 12.3f));

    //. x = -0 ; y = +Inf -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(fmodf(-0.0f, INFINITY));

    //. x = -0 ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(-0.0f, NAN));


    //. -- x = +0 --
    //. x = +0 ; y = -Inf -> x
    TEST_ASSERT_FLOAT_IS_ZERO(fmodf(0.0f, -INFINITY));

    //. x = +0 ; y < 0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(fmodf(0.0f, -12.3f));

    //. x = +0 ; y = -0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(0.0f, -0.0f));

    //. x = +0 ; y = +0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(0.0f, 0.0f));

    //. x = +0 ; y > 0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(fmodf(0.0f, 12.3f));

    //. x = +0 ; y = +Inf -> x
    TEST_ASSERT_FLOAT_IS_ZERO(fmodf(0.0f, INFINITY));

    //. x = +0 ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(0.0f, NAN));


    //. -- x > 0 --
    //. x > 0 ; y = -Inf -> x
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fmodf(1.23f, -INFINITY));

    //. x > 0 ; y < 0 -> fmod(x, y)
    TEST_ASSERT_EQUAL_FLOAT(12.3f, fmodf(12.3f, -123.0f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.89999600p3f, fmodf(123.0f, -12.3f));

    //. x > 0 ; y = -0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(1.23f, -0.0f));

    //. x > 0 ; y = +0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(1.23f, 0.0f));

    //. x > 0 ; y > 0 -> fmod(x, y)
    TEST_ASSERT_EQUAL_FLOAT(12.3f, fmodf(12.3f, 123.0f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.89999600p3f, fmodf(123.0f, 12.3f));

    //. x > 0 ; y = +Inf -> x
    TEST_ASSERT_EQUAL_FLOAT(1.23f, fmodf(1.23f, INFINITY));

    //. x > 0 ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(1.23f, NAN));


    //. -- x = +Inf --
    //. x = +Inf ; y = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(INFINITY, -INFINITY));

    //. x = +Inf ; y < 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(INFINITY, -12.3f));

    //. x = +Inf ; y = -0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(INFINITY, -0.0f));

    //. x = +Inf ; y = +0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(INFINITY, 0.0f));

    //. x = +Inf ; y > 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(INFINITY, 12.3f));

    //. x = +Inf ; y = +Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(INFINITY, INFINITY));

    //. x = +Inf ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(INFINITY, NAN));


    //. -- x = NaN --
    //. x = NaN ; y = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(NAN, -INFINITY));

    //. x = NaN ; y < 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(NAN, -12.3f));

    //. x = NaN ; y = -0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(NAN, -0.0f));

    //. x = NaN ; y = +0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(NAN, 0.0f));

    //. x = NaN ; y > 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(NAN, 12.3f));

    //. x = NaN ; y = +Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(NAN, INFINITY));

    //. x = NaN ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(fmodf(NAN, NAN));
}

void test_math_float_special_hypotf(void) {
    //. -- x = -Inf --
    //. x = -Inf ; y = -Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(hypotf(-INFINITY, -INFINITY));

    //. x = -Inf ; y e R -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(hypotf(-INFINITY, -123.0f));
    TEST_ASSERT_FLOAT_IS_INF(hypotf(-INFINITY, 0.0f));
    TEST_ASSERT_FLOAT_IS_INF(hypotf(-INFINITY, 123.0f));

    //. x = -Inf ; y = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(hypotf(-INFINITY, INFINITY));

    //. x = -Inf ; y = NaN -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(hypotf(-INFINITY, NAN));


    //. -- x e R --
    //. x e R ; y = -Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(hypotf(123.0f, -INFINITY));

    //. x e R ; y e R -> sqrt(x^2 + y^2)
    TEST_ASSERT_EQUAL_FLOAT(0x1.d84c2debp8f, hypotf(-456.0f, -123.0f));
    TEST_ASSERT_FLOAT_IS_ZERO(hypotf(0.0f, 0.0f));
    TEST_ASSERT_EQUAL_FLOAT(0x1.d84c2debp8f, hypotf(123.0f, 456.0f));

    //. x e R ; y = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(hypotf(123.0f, INFINITY));

    //. x e R ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(hypotf(123.0f, NAN));


    //. -- x = +Inf --
    //. x = +Inf ; y = -Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(hypotf(INFINITY, -INFINITY));

    //. x = +Inf ; y e R -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(hypotf(INFINITY, -123.0f));
    TEST_ASSERT_FLOAT_IS_INF(hypotf(INFINITY, 0.0f));
    TEST_ASSERT_FLOAT_IS_INF(hypotf(INFINITY, 123.0f));

    //. x = +Inf ; y = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(hypotf(INFINITY, INFINITY));

    //. x = +Inf ; y = NaN -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(hypotf(INFINITY, NAN));


    //. -- x = NaN --
    //. x = NaN ; y = -Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(hypotf(NAN, -INFINITY));

    //. x = NaN ; y e R -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(hypotf(NAN, -123.0f));
    TEST_ASSERT_FLOAT_IS_NAN(hypotf(NAN, 0.0f));
    TEST_ASSERT_FLOAT_IS_NAN(hypotf(NAN, 123.0f));

    //. x = NaN ; y = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(hypotf(NAN, INFINITY));

    //. x = NaN ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(hypotf(NAN, NAN));
}

void test_math_float_special_logf(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(logf(-INFINITY));

    //. x < 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(logf(-123.0f));

    //. x = -0 -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(logf(-0.0f));

    //. x = +0 -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(logf(0.0f));

    //. x e ]+0 ; +1[ -> log(x)
    TEST_ASSERT_EQUAL_FLOAT(-0x1.0c3baac8p1, logf(0.123f));

    //. x = +1 -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(logf(1.0f));

    //. x > +1 -> log(x)
    TEST_ASSERT_EQUAL_FLOAT(0x1.413a8c94p1, logf(12.3f));

    //. x = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(logf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(logf(NAN));
}

void test_math_float_special_log10f(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(log10f(-INFINITY));

    //. x < 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(log10f(-123.0f));

    //. x = -0 -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(log10f(-0.0f));

    //. x = +0 -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(log10f(0.0f));

    //. x e ]+0 ; +1[ -> log10(x)
    TEST_ASSERT_EQUAL_FLOAT(-0x1.d1f7f4a6p-1, log10f(0.123f));

    //. x = +1 -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(log10f(1.0f));

    //. x > +1 -> log10(x)
    TEST_ASSERT_EQUAL_FLOAT(0x1.17040596p0, log10f(12.3f));

    //. x = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(log10f(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(log10f(NAN));
}

void test_math_float_special_modff(void) {
    float i;

    //. x = -Inf -> -0 ; i = -Inf
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(modff(-INFINITY, &i));
    TEST_ASSERT_FLOAT_IS_NEG_INF(i);

    //. x < 0 -> x - fmod(x, 1) ; i = fmod(x, 1)
    TEST_ASSERT_EQUAL_FLOAT(-0x1.cccc0000p-2, modff(-123.45f, &i));
    TEST_ASSERT_EQUAL_FLOAT(-0x1.ec000000p6, i);

    //. x = -0 -> x - fmod(x, 1) ; i = fmod(x, 1)
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(modff(-0.0f, &i));
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(i);

    //. x = +0 -> x - fmod(x, 1) ; i = fmod(x, 1)
    TEST_ASSERT_FLOAT_IS_ZERO(modff(0.0f, &i));
    TEST_ASSERT_FLOAT_IS_ZERO(i);

    //. x > 0 -> x - fmod(x, 1) ; i = fmod(x, 1)
    TEST_ASSERT_EQUAL_FLOAT(0x1.6147c000p-2, modff(12.345f, &i));
    TEST_ASSERT_EQUAL_FLOAT(0x1.80000000p3, i);

    //. x = +Inf -> 0 ; i = +Inf
    TEST_ASSERT_FLOAT_IS_ZERO(modff(INFINITY, &i));
    TEST_ASSERT_FLOAT_IS_INF(i);

    //. x = NaN -> NaN ; i = NaN
    TEST_ASSERT_FLOAT_IS_NAN(modff(NAN, &i));
    TEST_ASSERT_FLOAT_IS_NAN(i);
}

void test_math_float_special_powf(void) {
    //. -- x = -Inf --
    //. x = -Inf ; y = -Inf -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(-INFINITY, -INFINITY));

    //. x = -Inf ; y e {2k + 1; k e Z<0} -> -0
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(powf(-INFINITY, -23.0f));

    //. x = -Inf ; y e {2k; k e Z<0} -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(-INFINITY, -12.0f));

    //. x = -Inf ; y < 0 ; y /e Z -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(-INFINITY, -123.45f));

    //. x = -Inf ; y = -0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(-INFINITY, -0.0f));

    //. x = -Inf ; y = +0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(-INFINITY, 0.0f));

    //. x = -Inf ; y > 0 ; y /e Z -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(-INFINITY, 123.45f));

    //. x = -Inf ; y e {2k; k e Z>0} -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(-INFINITY, 12.0f));

    //. x = -Inf ; y e {2k - 1; k e Z>0} -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(powf(-INFINITY, 23.0f));

    //. x = -Inf ; y = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(-INFINITY, INFINITY));

    //. x = -Inf ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(-INFINITY, NAN));


    //. -- x < -1 --
    //. x < -1 ; y = -Inf -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(-1.23f, -INFINITY));

    //. x < -1 ; y e {2k + 1; k e Z<0} -> x^y
    TEST_ASSERT_FLOAT_IS_ZERO(powf(-1.23f, -INFINITY));

    //. x < -1 ; y e {2k; k e Z<0} -> x^y
    TEST_ASSERT_FLOAT_IS_ZERO(powf(-1.23f, -INFINITY));

    //. x < -1 ; y < 0 ; y /e Z -> NaN
    TEST_ASSERT_FLOAT_IS_ZERO(powf(-1.23f, -INFINITY));

    //. x < -1 ; y = -0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(-1.23f, -0.0f));

    //. x < -1 ; y = +0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(-1.23f, 0.0f));

    //. x < -1 ; y > 0 ; y /e Z -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(-1.23f, 123.45f));

    //. x < -1 ; y e {2k; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.7fb7a1edp3f, powf(-1.23f, 12.0f));

    //. x < -1 ; y e {2k - 1; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(-0x1.d39a7be1p6f, powf(-1.23f, 23.0f));

    //. x < -1 ; y = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(-1.23f, INFINITY));

    //. x < -1 ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(-1.23f, NAN));


    //. -- x = -1 --
    //. x = -1 ; y = -Inf -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(-1.0f, -INFINITY));

    //. x = -1 ; y e {2k + 1; k e Z<0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(-0x1.00000000p0f, powf(-1.0f, -23.0f));

    //. x = -1 ; y e {2k; k e Z<0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.00000000p0f, powf(-1.0f, -12.0f));

    //. x = -1 ; y < 0 ; y /e Z -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(-1.0f, -123.45f));

    //. x = -1 ; y = -0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(-1.0f, -0.0f));

    //. x = -1 ; y = +0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(-1.0f, 0.0f));

    //. x = -1 ; y > 0 ; y /e Z -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(-1.0f, 123.45f));

    //. x = -1 ; y e {2k; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.00000000p0f, powf(-1.0f, 12.0f));

    //. x = -1 ; y e {2k - 1; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(-0x1.00000000p0f, powf(-1.0f, 23.0f));

    //. x = -1 ; y = +Inf -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(-1.0f, INFINITY));

    //. x = -1 ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(-1.0f, NAN));


    //. -- x e ]-1; -0[ --
    //. x e ]-1; -0[ ; y = -Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(-0.75f, -INFINITY));

    //. x e ]-1; -0[ ; y e {2k + 1; k e Z<0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(-0x1.75bb89a6p9f, powf(-0.75f, -23.0f));

    //. x e ]-1; -0[ ; y e {2k; k e Z<0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.f91bd1b6p4f, powf(-0.75f, -12.0f));

    //. x e ]-1; -0[ ; y < 0 ; y /e Z -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(-0.75f, -123.45f));

    //. x e ]-1; -0[ ; y = -0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(-0.75f, -0.0f));

    //. x e ]-1; -0[ ; y = +0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(-0.75f, 0.0f));

    //. x e ]-1; -0[ ; y > 0 ; y /e Z -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(-0.75f, 123.45f));

    //. x e ]-1; -0[ ; y e {2k; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.037e2000p-5f, powf(-0.75f, 12.0f));

    //. x e ]-1; -0[ ; y e {2k - 1; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(-0x1.5eb5ee84p-10f, powf(-0.75f, 23.0f));

    //. x e ]-1; -0[ ; y = +Inf -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(-0.75f, INFINITY));

    //. x e ]-1; -0[ ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(-0.75f, NAN));


    //. -- x = -0 --
    //. x = -0 ; y = -Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(-0.0f, -INFINITY));

    //. x = -0 ; y e {2k + 1; k e Z<0} -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(powf(-0.0f, -23.0f));

    //. x = -0 ; y e {2k; k e Z<0} -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(-0.0f, -12.0f));

    //. x = -0 ; y < 0 ; y /e Z -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(-0.0f, -123.45f));

    //. x = -0 ; y = -0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(-0.0f, -0.0f));

    //. x = -0 ; y = +0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(-0.0f, 0.0f));

    //. x = -0 ; y > 0 ; y /e Z -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(-0.0f, 123.45f));

    //. x = -0 ; y e {2k; k e Z>0} -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(-0.0f, 12.0f));

    //. x = -0 ; y e {2k - 1; k e Z>0} -> -0
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(powf(-0.0f, 23.0f));

    //. x = -0 ; y = +Inf -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(-0.0f, INFINITY));

    //. x = -0 ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(-0.0f, NAN));


    //. -- x = +0 --
    //. x = +0 ; y = -Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(0.0f, -INFINITY));

    //. x = +0 ; y e {2k + 1; k e Z<0} -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(0.0f, -23.0f));

    //. x = +0 ; y e {2k; k e Z<0} -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(0.0f, -12.0f));

    //. x = +0 ; y < 0 ; y /e Z -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(0.0f, -123.45f));

    //. x = +0 ; y = -0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(0.0f, -0.0f));

    //. x = +0 ; y = +0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(0.0f, 0.0f));

    //. x = +0 ; y > 0 ; y /e Z -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(0.0f, 123.45f));

    //. x = +0 ; y e {2k; k e Z>0} -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(0.0f, 12.0f));

    //. x = +0 ; y e {2k - 1; k e Z>0} -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(0.0f, 23.0f));

    //. x = +0 ; y = +Inf -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(0.0f, INFINITY));

    //. x = +0 ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(0.0f, NAN));


    //. -- x e ]+0; +1[ --
    //. x e ]+0; +1[ ; y = -Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(0.4f, -INFINITY));

    //. x e ]+0; +1[ ; y e {2k + 1; k e Z<0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.52d024e1p30f, powf(0.4f, -23.0f));

    //. x e ]+0; +1[ ; y e {2k; k e Z<0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.d1a944abp15f, powf(0.4f, -12.0f));

    //. x e ]+0; +1[ ; y < 0 ; y /e Z -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.327e7e35p16f, powf(0.4f, -12.3f));

    //. x e ]+0; +1[ ; y = -0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(0.4f, -0.0f));

    //. x e ]+0; +1[ ; y = +0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(0.4f, 0.0f));

    //. x e ]+0; +1[ ; y > 0 ; y /e Z -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.aba63a04p-17f, powf(0.4f, 12.3f));

    //. x e ]+0; +1[ ; y e {2k; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.19799b5fp-16f, powf(0.4f, 12.0f));

    //. x e ]+0; +1[ ; y e {2k - 1; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.82db3cb1p-31f, powf(0.4f, 23.0f));

    //. x e ]+0; +1[ ; y = +Inf -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(0.4f, INFINITY));

    //. x e ]+0; +1[ ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(0.4f, NAN));


    //. -- x = 1 --
    //. x = 1 ; y = -Inf -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(1.0f, -INFINITY));

    //. x = 1 ; y e {2k + 1; k e Z<0} -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(1.0f, -INFINITY));

    //. x = 1 ; y e {2k; k e Z<0} -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(1.0f, -INFINITY));

    //. x = 1 ; y < 0 ; y /e Z -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(1.0f, -INFINITY));

    //. x = 1 ; y = -0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(1.0f, -0.0f));

    //. x = 1 ; y = +0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(1.0f, 0.0f));

    //. x = 1 ; y > 0 ; y /e Z -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(1.0f, 123.45f));

    //. x = 1 ; y e {2k; k e Z>0} -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(1.0f, 12.0f));

    //. x = 1 ; y e {2k - 1; k e Z>0} -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(1.0f, 23.0f));

    //. x = 1 ; y = +Inf -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(1.0f, INFINITY));

    //. x = 1 ; y = NaN -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(1.0f, NAN));


    //. -- x > 1 --
    //. x > 1 ; y = -Inf -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(12.3f, -INFINITY));

    //. x > 1 ; y e {2k + 1; k e Z<0} -> x^y
    TEST_ASSERT_FLOAT_IS_ZERO(powf(12.3f, -INFINITY));

    //. x > 1 ; y e {2k; k e Z<0} -> x^y
    TEST_ASSERT_FLOAT_IS_ZERO(powf(12.3f, -INFINITY));

    //. x > 1 ; y < 0 ; y /e Z -> x^y
    TEST_ASSERT_FLOAT_IS_ZERO(powf(12.3f, -INFINITY));

    //. x > 1 ; y = -0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(12.3f, -0.0f));

    //. x > 1 ; y = +0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(12.3f, 0.0f));

    //. x > 1 ; y > 0 ; y /e Z -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.9ec31e00p44f, powf(12.3f, 12.345f));

    //. x > 1 ; y e {2k; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.5cfd263ap43f, powf(12.3f, 12.0f));

    //. x > 1 ; y e {2k - 1; k e Z>0} -> x^y
    TEST_ASSERT_EQUAL_FLOAT(0x1.356f1b2cp83f, powf(12.3f, 23.0f));

    //. x > 1 ; y = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(12.3f, INFINITY));

    //. x > 1 ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(12.3f, NAN));


    //. -- x = +Inf --
    //. x = +Inf ; y = -Inf -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(INFINITY, -INFINITY));

    //. x = +Inf ; y e {2k + 1; k e Z<0} -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(INFINITY, -INFINITY));

    //. x = +Inf ; y e {2k; k e Z<0} -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(INFINITY, -INFINITY));

    //. x = +Inf ; y < 0 ; y /e Z -> +0
    TEST_ASSERT_FLOAT_IS_ZERO(powf(INFINITY, -INFINITY));

    //. x = +Inf ; y = -0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(INFINITY, -0.0f));

    //. x = +Inf ; y = +0 -> +1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(INFINITY, 0.0f));

    //. x = +Inf ; y > 0 ; y /e Z -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(INFINITY, 123.45f));

    //. x = +Inf ; y e {2k; k e Z>0} -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(INFINITY, 12.0f));

    //. x = +Inf ; y e {2k - 1; k e Z>0} -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(INFINITY, 23.0f));

    //. x = +Inf ; y = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(powf(INFINITY, INFINITY));

    //. x = +Inf ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(INFINITY, NAN));


    //. -- x = NaN --
    //. x = NaN ; y = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(NAN, -INFINITY));

    //. x = NaN ; y e {2k + 1; k e Z<0} -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(NAN, -1.0f));

    //. x = NaN ; y e {2k; k e Z<0} -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(NAN, -2.0f));

    //. x = NaN ; y < 0 ; y /e Z  -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(NAN, -34.5f));

    //. x = NaN ; y = -0  -> 1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(NAN, -0.0f));

    //. x = NaN ; y = +0 -> 1
    TEST_ASSERT_EQUAL_FLOAT(1.0f, powf(NAN, 0.0f));

    //. x = NaN ; y > 0 ; y /e Z -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(NAN, 1.23f));

    //. x = NaN ; y e {2k; k e Z>0} -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(NAN, 4.0f));

    //. x = NaN ; y e {2k - 1; k e Z>0} -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(NAN, 5.0f));

    //. x = NaN ; y = +Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(NAN, INFINITY));

    //. x = NaN ; y = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(powf(NAN, NAN));
}


void test_math_float_special_rem2pif(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(rem2pif(-INFINITY));

    //. x < 0 -> rem2pi(x)
    TEST_ASSERT_EQUAL_FLOAT(-0x1.e95b14cep-2f, rem2pif(-123.0f));

    //. x = -0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(rem2pif(-0.0f));

    //. x = +0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(rem2pif(0.0f));

    //. x > 0 -> rem2pi(x)
    TEST_ASSERT_EQUAL_FLOAT(0x1.4deedb22p0f, rem2pif(12.3f));

    //. x = +Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(rem2pif(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(rem2pif(NAN));
}

void test_math_float_special_roundf(void) {
    //. x = -Inf -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(roundf(-INFINITY));

    //. x < 0 -> ceil(x - 0.5)
    TEST_ASSERT_EQUAL_FLOAT(-123.0f, roundf(-123.45f));
    TEST_ASSERT_EQUAL_FLOAT(-124.0f, roundf(-123.54f));

    //. x = -0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(roundf(-0.0f));

    //. x = +0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(roundf(0.0f));

    //. x > 0 -> floor(x + 0.5)
    TEST_ASSERT_EQUAL_FLOAT(12.0f, roundf(12.345f));
    TEST_ASSERT_EQUAL_FLOAT(13.0f, roundf(12.543f));

    //. x = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(roundf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(roundf(NAN));
}

void test_math_float_special_sinf(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(sinf(-INFINITY));

    //. x < 0 -> sin(x)
    TEST_ASSERT_EQUAL_FLOAT(0x1.99c7d7cbp-1f, sinf(-123.45f));

    //. x = -0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(sinf(-0.0f));

    //. x = +0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(sinf(0.0f));

    //. x > 0 -> sin(x)
    TEST_ASSERT_EQUAL_FLOAT(-0x1.cba848bcp-3f, sinf(12.34f));

    //. x = +Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(sinf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(sinf(NAN));
}

void test_math_float_special_sqrtf(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(sqrtf(-INFINITY));

    //. x < 0 -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(sqrtf(-123.45f));

    //. x = -0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(sqrtf(-0.0f));

    //. x = +0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(sqrtf(0.0f));

    //. x > 0 -> sqrt(x)
    TEST_ASSERT_EQUAL_FLOAT(0x1.c1a48856p1f, sqrtf(12.34f));

    //. x = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(sqrtf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(sqrtf(NAN));
}

void test_math_float_special_tanf(void) {
    //. x = -Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(tanf(-INFINITY));

    //. x < 0 -> tan(x)
    TEST_ASSERT_EQUAL_FLOAT(-0x1.55c080d6p0f, tanf(-123.45f));

    //. x = -0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(tanf(-0.0f));

    //. x = +0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(tanf(0.0f));

    //. x > 0 -> tan(x)
    TEST_ASSERT_EQUAL_FLOAT(-0x1.d7b100d2p-3f, tanf(12.34f));

    //. x = +Inf -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(tanf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(tanf(NAN));
}

void test_math_float_special_truncf(void) {
    //. x = -Inf -> -Inf
    TEST_ASSERT_FLOAT_IS_NEG_INF(truncf(-INFINITY));

    //. x < 0 -> ceil(x)
    TEST_ASSERT_EQUAL_FLOAT(-123.0f, truncf(-123.45f));
    TEST_ASSERT_EQUAL_FLOAT(-12.0f, truncf(-12.0f));

    //. x = -0 -> x
    TEST_ASSERT_FLOAT_IS_NEG_ZERO(truncf(-0.0f));

    //. x = +0 -> x
    TEST_ASSERT_FLOAT_IS_ZERO(truncf(0.0f));

    //. x > 0 -> floor(x)
    TEST_ASSERT_EQUAL_FLOAT(12.0f, truncf(12.34f));
    TEST_ASSERT_EQUAL_FLOAT(123.0f, truncf(123.0f));

    //. x = +Inf -> +Inf
    TEST_ASSERT_FLOAT_IS_INF(truncf(INFINITY));

    //. x = NaN -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(truncf(NAN));
}

void test_math_float_special_isfinite(void) {
    //. x = -Inf -> 0
    TEST_ASSERT_EQUAL_INT(0, isfinite(-INFINITY));

    //. x e R -> /0
    TEST_ASSERT_NOT_EQUAL(0, isfinite(-45.6f));
    TEST_ASSERT_NOT_EQUAL(0, isfinite(-0.0f));
    TEST_ASSERT_NOT_EQUAL(0, isfinite(0.0f));
    TEST_ASSERT_NOT_EQUAL(0, isfinite(0.123f));

    //. x = +Inf -> 0
    TEST_ASSERT_EQUAL_INT(0, isfinite(INFINITY));

    //. x = NaN -> 0
    TEST_ASSERT_EQUAL_INT(0, isfinite(NAN));
}

void test_math_float_special_isinf(void) {
    //. x = -Inf -> /0
    TEST_ASSERT_NOT_EQUAL(0, isinf(-INFINITY));

    //. x e R -> 0
    TEST_ASSERT_EQUAL_INT(0, isinf(-45.6f));
    TEST_ASSERT_EQUAL_INT(0, isinf(-0.0f));
    TEST_ASSERT_EQUAL_INT(0, isinf(0.0f));
    TEST_ASSERT_EQUAL_INT(0, isinf(0.123f));

    //. x = +Inf -> /0
    TEST_ASSERT_NOT_EQUAL(0, isinf(INFINITY));

    //. x = NaN -> 0
    TEST_ASSERT_EQUAL_INT(0, isinf(NAN));
}

void test_math_float_special_isnan(void) {
    //. x = -Inf -> 0
    TEST_ASSERT_EQUAL_INT(0, isnan(-INFINITY));

    //. x e R -> 0
    TEST_ASSERT_EQUAL_INT(0, isnan(-45.6f));
    TEST_ASSERT_EQUAL_INT(0, isnan(-0.0f));
    TEST_ASSERT_EQUAL_INT(0, isnan(0.0f));
    TEST_ASSERT_EQUAL_INT(0, isnan(0.123f));

    //. x = +Inf -> 0
    TEST_ASSERT_EQUAL_INT(0, isnan(INFINITY));

    //. x = NaN -> /0
    TEST_ASSERT_NOT_EQUAL(0, isnan(NAN));
}

void test_math_float_special_signbit(void) {
    //. x = -NaN -> /0
    TEST_ASSERT_NOT_EQUAL(0, signbit(-NAN));

    //. x = -Inf -> /0
    TEST_ASSERT_NOT_EQUAL(0, signbit(-INFINITY));

    //. x < 0 -> /0
    TEST_ASSERT_NOT_EQUAL(0, signbit(-123.0f));

    //. x = -0 -> /0
    TEST_ASSERT_NOT_EQUAL(0, signbit(-0.0f));

    //. x = +0 -> 0
    TEST_ASSERT_EQUAL_INT(0, signbit(0.0f));

    //. x > 0 -> 0
    TEST_ASSERT_EQUAL_INT(0, signbit(34.5f));

    //. x = +Inf -> 0
    TEST_ASSERT_EQUAL_INT(0, signbit(INFINITY));

    //. x = +NaN -> 0
    TEST_ASSERT_EQUAL_INT(0, signbit(NAN));
}

void test_math_float_special_fpclassify(void) {
    //. x = -Inf -> FP_INFINITE
    TEST_ASSERT_EQUAL(FP_INFINITE, fpclassify(-INFINITY));

    //. x < 0 -> FP_NORMAL
    TEST_ASSERT_EQUAL(FP_NORMAL, fpclassify(-123.0f));

    //. x e -Denormal -> FP_SUBNORMAL
    TEST_ASSERT_EQUAL(FP_SUBNORMAL, fpclassify(-0x1.0p-149f));

    //. x = -0 -> FP_ZERO
    TEST_ASSERT_EQUAL(FP_ZERO, fpclassify(-0.0f));

    //. x = +0 -> FP_ZERO
    TEST_ASSERT_EQUAL(FP_ZERO, fpclassify(0.0f));

    //. x e -Denormal -> FP_SUBNORMAL
    TEST_ASSERT_EQUAL(FP_SUBNORMAL, fpclassify(0x1.0p-149f));

    //. x > 0 -> FP_NORMAL
    TEST_ASSERT_EQUAL(FP_NORMAL, fpclassify(34.5f));

    //. x = +Inf -> FP_INFINITE
    TEST_ASSERT_EQUAL(FP_INFINITE, fpclassify(INFINITY));

    //. x = NaN -> FP_NAN
    TEST_ASSERT_EQUAL(FP_NAN, fpclassify(NAN));
}

void test_math_float_special_nanf(void) {
    //. -> NaN
    TEST_ASSERT_FLOAT_IS_NAN(nanf(""));
}

int main(void) {
    UNITY_BEGIN();

    RUN_TEST(test_math_float_special_acosf);
    RUN_TEST(test_math_float_special_asinf);
    RUN_TEST(test_math_float_special_atanf);
    RUN_TEST(test_math_float_special_atan2f);
    RUN_TEST(test_math_float_special_ceilf);
    RUN_TEST(test_math_float_special_copysignf);
    RUN_TEST(test_math_float_special_cosf);
    RUN_TEST(test_math_float_special_deg2radf);
    RUN_TEST(test_math_float_special_expf);
    RUN_TEST(test_math_float_special_fabsf);
    RUN_TEST(test_math_float_special_floorf);
    RUN_TEST(test_math_float_special_fmaxf);
    RUN_TEST(test_math_float_special_fminf);
    RUN_TEST(test_math_float_special_fmodf);
    RUN_TEST(test_math_float_special_hypotf);
    RUN_TEST(test_math_float_special_logf);
    RUN_TEST(test_math_float_special_log10f);
    RUN_TEST(test_math_float_special_modff);
    RUN_TEST(test_math_float_special_powf);
    RUN_TEST(test_math_float_special_rem2pif);
    RUN_TEST(test_math_float_special_roundf);
    RUN_TEST(test_math_float_special_sinf);
    RUN_TEST(test_math_float_special_sqrtf);
    RUN_TEST(test_math_float_special_tanf);
    RUN_TEST(test_math_float_special_truncf);
    RUN_TEST(test_math_float_special_isfinite);
    RUN_TEST(test_math_float_special_isinf);
    RUN_TEST(test_math_float_special_isnan);
    RUN_TEST(test_math_float_special_signbit);
    RUN_TEST(test_math_float_special_fpclassify);
    RUN_TEST(test_math_float_special_nanf);

    return UNITY_END();
}
